#!/bin/bash

docker login registry.gitlab.com
docker build -t registry.gitlab.com/volunteer-conversation/volunteer-conversation-app .
docker push registry.gitlab.com/volunteer-conversation/volunteer-conversation-app