FROM circleci/android:api-29-node
RUN gem install fastlane
RUN mkdir $HOME/.keystore
COPY .keystore/vcs.jks $HOME/.keystore/vcs.jks
