package org.volunteer.conversation

import com.google.gson.JsonElement
import com.google.gson.JsonParser
import org.junit.Assert.assertEquals
import org.junit.Rule
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.junit.MockitoJUnit
import org.volunteer.conversation.chatbot.dtos.QueryResultParametersDeserializer


class QueryResultParameterDeserializerUnitTest : BaseUnitTest() {

    @Rule
    @JvmField
    var mockitoRule = MockitoJUnit.rule()

    @InjectMocks
    lateinit var queryResultParameterDeserializer: QueryResultParametersDeserializer

    @Test
    fun `GIVEN a JSON WHEN name is available THEN it should be parsed`() {
        val name = "hans"
        val json = JsonParser().parse("{\"name\": \"$name\"}")

        val queryResultParameters = deserialize(json)

        assertEquals(name, queryResultParameters.name)
    }

    @Test
    fun `GIVEN a JSON WHEN report is available THEN it should be parsed`() {
        val report = "COVID-19"
        val json = JsonParser().parse("{\"report\": \"$report\"}")

        val queryResultParameters = deserialize(json)

        assertEquals(report, queryResultParameters.report)
    }

    @Test
    fun `GIVEN a JSON WHEN report is not available THEN an empty string should be set`() {
        val json = JsonParser().parse("{}")

        val queryResultParameters = deserialize(json)

        val emptyString = ""
        assertEquals(emptyString, queryResultParameters.report)
    }

    @Test
    fun `GIVEN a JSON WHEN location is a string THEN it should be parsed as city`() {
        val locationString = "vienna"
        val json = JsonParser().parse("{\"location\": \"$locationString\"}")

        val queryResultParameters = deserialize(json)

        assertEquals(locationString, queryResultParameters.location.city)
    }

    @Test
    fun `GIVEN a JSON WHEN location an object THEN it should be parsed as object`() {
        val locationString = "vienna"
        val json = JsonParser().parse("{\"location\": {\"city\": \"$locationString\"}}")

        val queryResultParameters = deserialize(json)

        assertEquals(locationString, queryResultParameters.location.city)
    }

    @Test
    fun `GIVEN a JSON WHEN it contains unknown properties THEN this properties should be concatenated to a string`() {
        val anotherPropertyValue = "anotherPropertyValue"
        val anotherPropertyKey = "anotherPropertyKey"
        val json = JsonParser().parse("{\"$anotherPropertyKey\": \"$anotherPropertyValue\"}")

        val queryResultParameters = deserialize(json)

        assertEquals(
            anotherPropertyValue,
            queryResultParameters.unknownProperties.asJsonObject.get(anotherPropertyKey).asString
        )
    }

    private fun deserialize(json: JsonElement) =
        queryResultParameterDeserializer.deserialize(json = json, context = null, typeOfT = null)
}