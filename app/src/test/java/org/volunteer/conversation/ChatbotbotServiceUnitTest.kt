package org.volunteer.conversation

import kotlinx.coroutines.runBlocking
import org.junit.Assert.*
import org.junit.Rule
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnit
import org.volunteer.conversation.auth.AuthService
import org.volunteer.conversation.chatbot.ChatbotRepository
import org.volunteer.conversation.chatbot.ChatbotService
import org.volunteer.conversation.chatbot.dtos.QueryResult
import org.volunteer.conversation.chatbot.dtos.QueryResultLocation
import org.volunteer.conversation.chatbot.dtos.QueryResultParameters
import org.volunteer.conversation.sms.dtos.IncomingMessage


class ChatbotbotServiceUnitTest : BaseUnitTest() {
    companion object {
        const val PHONE_NUMBER = "06768499557"
        const val MESSAGE = "this is a message"
    }

    @Rule
    @JvmField
    var mockitoRule = MockitoJUnit.rule()

    @Mock
    lateinit var chatbotRepository: ChatbotRepository

    @Mock
    lateinit var authService: AuthService

    @InjectMocks
    lateinit var chatbotService: ChatbotService

    @Test
    fun `GIVEN a message WHEN is passed to the chatbot service THEN service should query the repository for answers`() =
        runBlocking<Unit> {
            val fakedQueryResult =
                QueryResult("", QueryResultParameters(QueryResultLocation(""), ""), false)

            Mockito.`when`(chatbotRepository.getAnswer(PHONE_NUMBER, MESSAGE))
                .thenReturn(fakedQueryResult)

            val queryResult = chatbotService.getQueryResult(
                IncomingMessage(
                    PHONE_NUMBER,
                    MESSAGE
                )
            )

            assertEquals(queryResult, fakedQueryResult)
            verify(chatbotRepository, times(1)).getAnswer(PHONE_NUMBER, MESSAGE)
        }

    @Test
    fun `GIVEN a chatbot query result WHEN name is empty THEN it should still be evaluated to be a full report`() {
        val queryResult =
            QueryResult(
                parameters = QueryResultParameters(name = "", report = "COVID-19"),
                allRequiredParamsPresent = true
            )

        assertTrue(chatbotService.isFullConversation(queryResult))
    }

    @Test
    fun `GIVEN a chatbot query result WHEN report (type) is empty THEN it should not be evaluated to be a full report`() {
        val queryResult =
            QueryResult(
                parameters = QueryResultParameters(name = "Hans", report = ""),
                allRequiredParamsPresent = true
            )

        assertFalse(chatbotService.isFullConversation(queryResult))
    }

    @Test
    fun `GIVEN a chatbot query result WHEN not all required parameters are present THEN it should not be evaluated to be a full report`() {
        val queryResult =
            QueryResult(
                parameters = QueryResultParameters(name = "Hans", report = "COVID-19"),
                allRequiredParamsPresent = false
            )

        assertFalse(chatbotService.isFullConversation(queryResult))
    }

    @Test
    fun `GIVEN a chatbot query result WHEN name and report is set and all required parameters are present THEN it should be evaluated to be a full report`() {
        assertTrue(chatbotService.isFullConversation(getFullConversation()))
    }

    @Test
    fun `GIVEN a chatbot query result WHEN it is a full report THEN it should not be evaluated to be a location update`() {
        assertFalse(chatbotService.isLocationUpdate(getFullConversation()))
    }

    @Test
    fun `GIVEN a chatbot query result WHEN location is not set THEN it should not be evaluated to be a location update`() {
        val queryResult =
            QueryResult(
                parameters = QueryResultParameters(
                    location = QueryResultLocation(city = ""),
                    name = "Hans",
                    report = "COVID-19"
                ), allRequiredParamsPresent = true
            )

        assertFalse(chatbotService.isLocationUpdate(queryResult))
    }

    @Test
    fun `GIVEN a chatbot query result WHEN it is not a full report and location is set THEN it should be evaluated to be a location update`() {
        val queryResult =
            QueryResult(parameters = QueryResultParameters(location = QueryResultLocation(city = "Vienna")))

        assertTrue(chatbotService.isLocationUpdate(queryResult))
    }

    private fun getFullConversation() = QueryResult(
        parameters = QueryResultParameters(
            location = QueryResultLocation(
                city =
                ""
            ), name = "Hans", report = "COVID-19"
        ), allRequiredParamsPresent = true
    )
}