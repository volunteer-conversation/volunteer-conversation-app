package org.volunteer.conversation

import com.nhaarman.mockitokotlin2.stub
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnit
import org.mockito.junit.MockitoRule
import org.volunteer.conversation.persistence.ConversationDatabase
import org.volunteer.conversation.persistence.VolunteerRepositoryImplementation
import org.volunteer.conversation.persistence.daos.ConversationDao
import org.volunteer.conversation.persistence.daos.VolunteerDao
import org.volunteer.conversation.persistence.entities.*

class VolunteerRepositoryImplementationUnitTest : BaseUnitTest() {
    @Rule
    @JvmField
    var mockitoRule: MockitoRule = MockitoJUnit.rule()

    @Mock
    lateinit var conversationDatabase: ConversationDatabase

    @Mock
    lateinit var conversationDao: ConversationDao

    @Mock
    lateinit var volunteerDao: VolunteerDao

    @InjectMocks
    lateinit var volunteerRepositoryImplementation: VolunteerRepositoryImplementation

    private val volunteer = Volunteer(id = 1L, name = "Peter", number = "+436760395582")
    private val conversation =
        Conversation(id = 1L, subject = Subject.REPORT, incident = "flood", locationName = "Vienna")

    @Before
    fun before() {
        Mockito.`when`(conversationDatabase.conversationDao()).thenReturn(conversationDao)
        Mockito.`when`(conversationDatabase.volunteerDao()).thenReturn(volunteerDao)
    }

    //TODO: I think it makes more sense to perform test like this directly on the dao with an
    // in-memory DB since no extra logic or data transformation is done in the repository.
    @Test
    fun `GEVEN a volunteer and a conversation exists WHEN they are queried THEN they should be returned`() =
        runBlocking {
            stubVolunteerDao(volunteer, conversation)

            val volunteerWithConversation =
                volunteerRepositoryImplementation.getVolunteerWithConversations()

            volunteerWithConversation.collect {
                assertEquals(1, it.size)
                assertEquals(1, it.first().conversations.size)
                assertEquals(volunteer, it.first().volunteer)
                assertEquals(conversation, it.first().conversations.first())
            }
        }

    @Test
    fun `GEVEN no volunteer or conversation exists WHEN they are queried THEN an empty list should be returned`() =
        runBlocking {
            stubEmptyVolunteerDao()

            val volunteerWithConversation =
                volunteerRepositoryImplementation.getVolunteerWithConversations()

            volunteerWithConversation.collect {
                assertEquals(0, it.size)
            }
        }

    @Test
    fun `GEVEN a volunteer and a conversation exists WHEN they are queried via getVolunteerLocations THEN they should be returned order by locations`() =
        runBlocking {
            stubConversationDao(conversation, volunteer)

            val volunteerLocations = volunteerRepositoryImplementation.getVolunteerLocations()

            volunteerLocations.collect {
                assertEquals(1, it.size)
                assertEquals(conversation.locationName, it.first().locationName)
                assertEquals(1, it.first().volunteers.size)
                assertEquals(volunteer, it.first().volunteers.first())
            }
        }

    private fun stubVolunteerDao(volunteer: Volunteer, conversation: Conversation) {
        volunteerDao.stub {
            onBlocking {
                getVolunteerWithConversation()
            }.thenReturn(
                flowOf(
                    listOf(
                        VolunteerWithConversations(
                            volunteer,
                            listOf(conversation)
                        )
                    )
                )
            )
        }
    }

    private fun stubEmptyVolunteerDao() {
        volunteerDao.stub {
            onBlocking {
                getVolunteerWithConversation()
            }.thenReturn(flowOf())
        }
    }

    private fun stubConversationDao(
        conversation: Conversation,
        volunteer: Volunteer = this.volunteer
    ) {
        conversationDao.stub {
            onBlocking {
                getConversationWithVolunteers()
            }.thenReturn(flowOf(listOf(ConversationWithVolunteer(conversation, volunteer))))
        }
    }
}