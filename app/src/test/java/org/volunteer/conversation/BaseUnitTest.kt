package org.volunteer.conversation

import org.junit.BeforeClass
import org.mockito.Mockito
import org.volunteer.conversation.sms.dtos.IncomingMessage
import org.volunteer.conversation.util.Log

abstract class BaseUnitTest {
    companion object {
        @JvmStatic
        @BeforeClass
        fun beforeAll() {
            Log.shouldMock = true
        }
    }

    protected val volunteerPhoneNumber = "6505551212"
    protected val volunteerRequest = "Hi, I want to conversation something."
    protected val volunteerSms = IncomingMessage(
        volunteerPhoneNumber,
        volunteerRequest
    )

    protected val randomPhoneNumber = "6502828282"
    protected val randomRequest = "Hi, how was your day?"
    protected val randomSms =
        IncomingMessage(randomPhoneNumber, randomRequest)

    // Kotlin and Mockito have their issues, see https://medium.com/@elye.project/befriending-kotlin-and-mockito-1c2e7b0ef791

    fun <T> kotlinMockitoAnyFix(type: Class<T>): T {
        Mockito.any(type)
        return kotlinMockitoUninitializedFix()
    }

    fun <String> kotlinMockitoEqFix(text: String): String {
        Mockito.eq(text)
        return kotlinMockitoUninitializedFix()
    }

    fun <T> kotlinMockitoUninitializedFix(): T = null as T
}