package org.volunteer.conversation

import com.nhaarman.mockitokotlin2.capture
import com.nhaarman.mockitokotlin2.eq
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentCaptor
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.times
import org.mockito.junit.MockitoJUnit
import org.volunteer.conversation.auth.AuthService
import org.volunteer.conversation.chatbot.ChatbotRepository
import org.volunteer.conversation.chatbot.DialogflowWebservice
import org.volunteer.conversation.chatbot.dtos.*


class ChatbotRepositoryUnitTest : BaseUnitTest() {
    companion object {
        const val SESSION = "+436768599403"
        const val dialogflowChatbotId = ""
    }

    @Rule
    @JvmField
    var mockitoRule = MockitoJUnit.rule()

    @Mock
    lateinit var dialogflowWebservice: DialogflowWebservice

    @Mock
    lateinit var authService: AuthService

    @InjectMocks
    lateinit var chatbotRepository: ChatbotRepository

    @Test
    fun `GIVEN a question and a session WHEN is passed to the chatbot repository THEN the repository should return a query result`() =
        runBlocking<Unit> {
            val mockedQueryResult =
                QueryResult("", QueryResultParameters(QueryResultLocation(""), ""), false)

            mockDialogflowWebserviceDetectIntent()

            val queryResult = chatbotRepository.getAnswer(SESSION, "")
            assertEquals(queryResult, mockedQueryResult)
        }

    @Test
    fun `GIVEN a question and a session WHEN is passed to the chatbot repository THEN the repository should use the auth services access token`() =
        runBlocking<Unit> {
            val mockedAccessToken = "a mocked access token"

            Mockito.`when`(authService.accessToken).thenReturn(mockedAccessToken)
            mockDialogflowWebserviceDetectIntent()

            chatbotRepository.getAnswer(SESSION, "")
            Mockito.verify(dialogflowWebservice, times(1))
                .detectIntent(
                    kotlinMockitoEqFix("Bearer $mockedAccessToken"),
                    eq(SESSION),
                    eq(dialogflowChatbotId),
                    kotlinMockitoAnyFix(DetectIntentRequest::class.java)
                )
        }

    @Test
    fun `GIVEN a question and a session WHEN is passed to the chatbot repository THEN the chatbot service should construct a request using the question`() =
        runBlocking<Unit> {
            val mockedText = "I want to create an alert"
            val argument = ArgumentCaptor.forClass(DetectIntentRequest::class.java)
            mockDialogflowWebserviceDetectIntent()

            chatbotRepository.getAnswer(SESSION, mockedText)

            Mockito.verify(dialogflowWebservice).detectIntent(
                kotlinMockitoAnyFix(String::class.java),
                eq(SESSION),
                eq(dialogflowChatbotId),
                capture<DetectIntentRequest>(argument)
            )
            assertEquals(argument.value.queryInput.text.text, mockedText)
        }

    private fun mockDialogflowWebserviceDetectIntent() = runBlocking<Unit> {
        val mockedQueryResult =
            QueryResult("", QueryResultParameters(QueryResultLocation(""), ""), false)
        val mockedDetectIntentResponse = DetectIntentResponse(mockedQueryResult)
        Mockito.`when`(
            dialogflowWebservice.detectIntent(
                kotlinMockitoAnyFix(String::class.java),
                eq(SESSION),
                eq(dialogflowChatbotId),
                kotlinMockitoAnyFix(DetectIntentRequest::class.java)
            )
        ).thenReturn(mockedDetectIntentResponse)
    }
}