package org.volunteer.conversation

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.eq
import com.nhaarman.mockitokotlin2.timeout
import kotlinx.coroutines.runBlocking
import org.junit.Rule
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnit
import org.mockito.verification.VerificationMode
import org.volunteer.conversation.chatbot.ChatbotService
import org.volunteer.conversation.chatbot.dtos.QueryResult
import org.volunteer.conversation.chatbot.dtos.QueryResultLocation
import org.volunteer.conversation.chatbot.dtos.QueryResultParameters
import org.volunteer.conversation.persistence.ConversationRepository
import org.volunteer.conversation.persistence.SmsRepository
import org.volunteer.conversation.persistence.entities.Conversation
import org.volunteer.conversation.persistence.entities.Volunteer
import org.volunteer.conversation.sms.CustomSmsManager
import org.volunteer.conversation.sms.MessageChatbotConnector
import org.volunteer.conversation.sms.MessageProcessor
import org.volunteer.conversation.sms.dtos.IncomingMessage


class MessageChatbotConnectorUnitTest : BaseUnitTest() {
    @Rule
    @JvmField
    var mockitoRule = MockitoJUnit.rule()

    @Mock
    lateinit var conversationRepository: ConversationRepository

    @Mock
    lateinit var customSmsManager: CustomSmsManager

    @Mock
    lateinit var messageProcessor: MessageProcessor

    @Mock
    lateinit var chatbotService: ChatbotService

    @Mock
    lateinit var smsRepository: SmsRepository

    @InjectMocks
    lateinit var messageChatbotConnector: MessageChatbotConnector

    private var calledReceive = 0

    @Test
    fun `GIVEN receives message WHEN chatbotService is available THEN SMS should be sent`() =
        runBlocking<Unit> {
            mockMessageProcessorAndChatbotService()

            messageChatbotConnector.listenAndRespondToSms()

            Mockito.verify(customSmsManager, timeout(1000).atLeastOnce())
                .sendTextMessage(any(), any())
        }

    @Test
    fun `GIVEN receives message WHEN chatbotService is available THEN chatbot response should be sent to user`() =
        runBlocking<Unit> {
            val outgoingText = "Yeah, I'll do that for you"
            mockMessageProcessorAndChatbotService(outgoingText = outgoingText)

            messageChatbotConnector.listenAndRespondToSms()

            Mockito.verify(customSmsManager, timeout(1000).atLeastOnce())
                .sendTextMessage(any(), eq(outgoingText))
        }

    @Test
    fun `GIVEN receives message from a given user WHEN chatbotService is available THEN the given user should be responded to`() =
        runBlocking<Unit> {
            val phoneNumber = "+436768574896"
            mockMessageProcessorAndChatbotService(phoneNumber = phoneNumber)

            messageChatbotConnector.listenAndRespondToSms()

            Mockito.verify(customSmsManager, timeout(1000).atLeastOnce())
                .sendTextMessage(eq(phoneNumber), any())
        }

    @Test
    fun `GIVEN receives N message WHEN chatbotService is available THEN N responses should be sent`() =
        runBlocking<Unit> {
            messageChatbotConnector.listenAndRespondToSms()
            Mockito.verify(customSmsManager, timeout(1000).times(calledReceive))
                .sendTextMessage(any(), any())
        }

    @Test
    fun `GIVEN receives message WHEN chatbot service detects full conversation THEN it should persist the conversation`() =
        runBlocking {
            mockFullConversation()
            mockMessageProcessorAndChatbotService()

            messageChatbotConnector.listenAndRespondToSms()

            verifyFullConversationWasUpdated(timeout(1000).times(1))
        }

    @Test
    fun `GIVEN receives message WHEN chatbot service detects location update THEN it should persist the location of this volunteer's of the latest conversation`() =
        runBlocking {
            mockLocationUpdate()
            mockMessageProcessorAndChatbotService()

            messageChatbotConnector.listenAndRespondToSms()

            verifyLocationWasUpdated(timeout(1000).times(1))
        }

    @Test
    fun `GIVEN receives message WHEN chatbot service detects no full conversation nor location update THEN it should not persist anything`() =
        runBlocking {
            mockMessageProcessorAndChatbotService()

            messageChatbotConnector.listenAndRespondToSms()

            verifyLocationWasUpdated(timeout(1000).times(0))
        }

    private suspend fun mockMessageProcessorAndChatbotService(
        phoneNumber: String = "",
        incomingText: String = "",
        outgoingText: String = ""
    ) {
        val mockedIncomingMessage = IncomingMessage(phoneNumber, incomingText)
        val mockedQueryResult =
            QueryResult(outgoingText, QueryResultParameters(QueryResultLocation(""), ""), false)

        Mockito.`when`(messageProcessor.receive())
            .thenReturn(mockedIncomingMessage)
            .then {
                calledReceive++
            }

        Mockito.`when`(chatbotService.getQueryResult(mockedIncomingMessage))
            .thenReturn(mockedQueryResult)
    }

    private fun mockLocationUpdate() {
        Mockito.`when`(chatbotService.isLocationUpdate(any()))
            .thenReturn(true)
    }

    private fun mockFullConversation() {
        Mockito.`when`(chatbotService.isFullConversation(any()))
            .thenReturn(true)
    }

    private suspend fun verifyLocationWasUpdated(verificationMode: VerificationMode) {
        Mockito.verify(conversationRepository, verificationMode)
            .insertLocationForLastConversationIfVolunteerExists(
                kotlinMockitoAnyFix(Volunteer::class.java),
                kotlinMockitoAnyFix(QueryResultLocation::class.java)
            )
    }

    private suspend fun verifyFullConversationWasUpdated(verificationMode: VerificationMode) {
        Mockito.verify(conversationRepository, verificationMode).insertConversation(
            kotlinMockitoAnyFix(Volunteer::class.java),
            kotlinMockitoAnyFix(Conversation::class.java)
        )
    }
}