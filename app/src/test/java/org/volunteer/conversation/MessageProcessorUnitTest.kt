package org.volunteer.conversation

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.junit.Assert.assertEquals
import org.junit.Test
import org.volunteer.conversation.sms.MessageProcessor
import org.volunteer.conversation.sms.dtos.IncomingMessage


class MessageProcessorUnitTest : BaseUnitTest() {
    val messageProcessor = MessageProcessor()

    @Test
    fun `GIVEN a coroutine that listens to incoming messages WHEN SMS is sent THEN the text should be emitted`() {
        lateinit var incomingMessage: IncomingMessage
        GlobalScope.launch {
            while (true) {
                incomingMessage = messageProcessor.receive()
            }
        }

        sendToProcessorAndWait(volunteerSms)

        assertEquals(volunteerSms, incomingMessage)
    }

    @Test
    fun `GIVEN a coroutine that listens to incoming messages WHEN 2 SMS are sent THEN the texts of both should be emitted with preserved order`() {
        val receivedMessages = mutableListOf<IncomingMessage>()
        GlobalScope.launch {
            while (true) {
                val message = messageProcessor.receive()
                message.let { receivedMessages.add(message) }
            }
        }

        sendToProcessorAndWait(volunteerSms)
        sendToProcessorAndWait(randomSms)

        assertEquals(volunteerSms, receivedMessages[0])
        assertEquals(randomSms, receivedMessages[1])
    }

    private fun sendToProcessorAndWait(incomingMessage: IncomingMessage) {
        messageProcessor.sendMessage(incomingMessage)
        Thread.sleep(100)
    }

    @Test
    fun `GIVEN a coroutine that listens to incoming messages WHEN 3 SMS are sent nearly at the same time THEN the texts of all should be emitted with preserved order`() {
        val receivedMessages = mutableListOf<IncomingMessage>()
        GlobalScope.launch {
            while (true) {
                val message = messageProcessor.receive()
                message.let { receivedMessages.add(message) }
            }
        }

        messageProcessor.sendMessage(volunteerSms)
        Thread.sleep(10)
        messageProcessor.sendMessage(randomSms)
        Thread.sleep(10)
        messageProcessor.sendMessage(volunteerSms)
        Thread.sleep(100)

        assertEquals(volunteerSms, receivedMessages[0])
        assertEquals(randomSms, receivedMessages[1])
        assertEquals(volunteerSms, receivedMessages[2])
    }
}
