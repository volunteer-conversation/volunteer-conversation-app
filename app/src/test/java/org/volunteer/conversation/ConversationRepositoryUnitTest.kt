package org.volunteer.conversation

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.never
import com.nhaarman.mockitokotlin2.stub
import com.nhaarman.mockitokotlin2.times
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotSame
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnit
import org.mockito.junit.MockitoRule
import org.mockito.verification.VerificationMode
import org.volunteer.conversation.chatbot.dtos.QueryResultLocation
import org.volunteer.conversation.persistence.ConversationDatabase
import org.volunteer.conversation.persistence.ConversationRepository
import org.volunteer.conversation.persistence.daos.ConversationDao
import org.volunteer.conversation.persistence.daos.VolunteerDao
import org.volunteer.conversation.persistence.entities.Conversation
import org.volunteer.conversation.persistence.entities.ConversationWithVolunteer
import org.volunteer.conversation.persistence.entities.Subject
import org.volunteer.conversation.persistence.entities.Volunteer
import java.util.*

class ConversationRepositoryUnitTest : BaseUnitTest() {
    @Rule
    @JvmField
    var mockitoRule: MockitoRule = MockitoJUnit.rule()

    @Mock
    lateinit var conversationDatabase: ConversationDatabase

    @Mock
    lateinit var conversationDao: ConversationDao

    @Mock
    lateinit var volunteerDao: VolunteerDao

    @InjectMocks
    lateinit var conversationRepository: ConversationRepository

    private val volunteer = Volunteer(id = 1L, name = "Peter", number = "+436760395582")
    private val conversation =
        Conversation(id = 1L, subject = Subject.REPORT, incident = "flood", locationName = "Vienna")
    private val location = QueryResultLocation(city = "Graz")

    @Before
    fun before() {
        Mockito.`when`(conversationDatabase.conversationDao()).thenReturn(conversationDao)
        Mockito.`when`(conversationDatabase.volunteerDao()).thenReturn(volunteerDao)
    }

    @Test
    fun `GIVEN conversations exist WHEN conversation with volunteers are requests THEN they should be returned`() =
        runBlocking {
            stubConversationDao(conversation, volunteer)

            val conversationWithVolunteer =
                conversationRepository.getConversationWithVolunteerSortedByCreation()

            conversationWithVolunteer.collect {
                assertEquals(it.size, 1)
                assertEquals(it.first().conversation, conversation)
                assertEquals(it.first().volunteer, volunteer)
            }
        }

    @Test
    fun `GIVEN a volunteer and a conversation WHEN the volunteer does already exist THEN volunteer should be fetched from DB`() =
        runBlocking {
            stubVolunteerDao(volunteer)

            conversationRepository.insertConversation(volunteer, conversation)

            verifyVolunteerReceivedByNumber(volunteer, times(1))
        }

    @Test
    fun `GIVEN a volunteer and a conversation WHEN the volunteer does not exist THEN volunteer should not be fetched from DB`() =
        runBlocking {
            stubEmptyVolunteerDao()

            conversationRepository.insertConversation(volunteer, conversation)

            verifyVolunteerReceivedByNumber(volunteer, never())
        }

    @Test
    fun `GIVEN a volunteer and a conversation THEN the conversation should be created`() =
        runBlocking<Unit> {
            stubVolunteerDao(volunteer)

            conversationRepository.insertConversation(volunteer, conversation)

            Mockito.verify(conversationDao, times(1)).insertConversation(conversation)
        }

    @Test
    fun `GIVEN a volunteer and a conversation THEN the conversation should be linked to the volunteer`() =
        runBlocking {
            stubVolunteerDao(volunteer)

            conversationRepository.insertConversation(volunteer, conversation)

            assertEquals(conversation.volunteerId, volunteer.id)
        }

    @Test
    fun `GIVEN a volunteer and a location WHEN the volunteer does already exist THEN the latest conversation of this volunteer should be updated`() =
        runBlocking {
            stubVolunteerDao(volunteer)
            stubConversationDao(conversation)

            assertNotSame(conversation.locationName, location.city)

            conversationRepository.insertLocationForLastConversationIfVolunteerExists(
                volunteer,
                location
            )

            assertEquals(conversation.locationName, location.city)
            verifyConversationWasUpdate(conversation, times(1))
        }

    @Test
    fun `GIVEN a volunteer and a location WHEN the volunteer does not exist THEN no conversation should not be updated`() =
        runBlocking {
            stubEmptyVolunteerDao()

            conversationRepository.insertLocationForLastConversationIfVolunteerExists(
                volunteer,
                location
            )

            verifyConversationWasUpdate(conversation, never())
        }

    @Test
    fun `GIVEN a volunteer and a location WHEN the volunteer does already exist, but it has no conversation THEN no conversation should be updated or created`() =
        runBlocking {
            stubVolunteerDao(volunteer)
            stubEmptyConversationDao()

            conversationRepository.insertLocationForLastConversationIfVolunteerExists(
                volunteer,
                location
            )

            verifyConversationWasUpdate(conversation, never())
        }

    private suspend fun stubVolunteerDao(volunteer: Volunteer) {
        volunteerDao.stub {
            onBlocking {
                insertVolunteer(volunteer)
            }.thenReturn(-1L)
        }
        Mockito.`when`(volunteerDao.getVolunteerByNumber(volunteer.number))
            .thenReturn(Optional.of(volunteer))
    }

    private suspend fun stubEmptyVolunteerDao() {
        volunteerDao.stub {
            onBlocking {
                insertVolunteer(any())
            }.thenReturn(1L)
        }
        Mockito.`when`(volunteerDao.getVolunteerByNumber(volunteer.number))
            .thenReturn(Optional.empty())
    }

    private suspend fun stubEmptyConversationDao() {
        conversationDao.stub {
            onBlocking {
                getLatestConversation(any())
            }.thenReturn(Optional.empty())
        }
        Mockito.`when`(volunteerDao.getVolunteerByNumber(volunteer.number))
            .thenReturn(
                Optional.of(
                    Volunteer(
                        id = volunteer.id,
                        number = volunteer.number,
                        name = volunteer.name
                    )
                )
            )
    }

    private suspend fun verifyVolunteerReceivedByNumber(
        volunteer: Volunteer,
        verificationMode: VerificationMode
    ) {
        Mockito.verify(volunteerDao, verificationMode).getVolunteerByNumber(volunteer.number)
    }

    private suspend fun stubConversationDao(
        conversation: Conversation,
        volunteer: Volunteer = this.volunteer
    ) {
        conversationDao.stub {
            onBlocking {
                getLatestConversation(any())
            }.thenReturn(Optional.of(conversation))

            onBlocking {
                getConversationWithVolunteers()
            }.thenReturn(flowOf(listOf(ConversationWithVolunteer(conversation, volunteer))))
        }
    }

    private suspend fun verifyConversationWasInserted(
        conversation: Conversation,
        verificationMode: VerificationMode
    ) {
        Mockito.verify(conversationDao, verificationMode).insertConversation(conversation)
    }

    private suspend fun verifyConversationWasUpdate(
        conversation: Conversation,
        verificationMode: VerificationMode
    ) {
        Mockito.verify(conversationDao, verificationMode).updateConversation(conversation)
    }
}