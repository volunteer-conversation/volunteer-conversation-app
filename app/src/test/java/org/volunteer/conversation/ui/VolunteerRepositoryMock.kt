package org.volunteer.conversation.ui

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOf
import org.volunteer.conversation.persistence.VolunteerRepository
import org.volunteer.conversation.persistence.entities.LocationWithVolunteers
import org.volunteer.conversation.persistence.entities.Volunteer
import org.volunteer.conversation.persistence.entities.VolunteerWithConversations
import org.volunteer.conversation.persistence.entities.VolunteerWithSms

class VolunteerRepositoryMock : VolunteerRepository {
    var volunteerLocation: Flow<List<LocationWithVolunteers>>? = null
    override fun getVolunteers(): Flow<List<Volunteer>> {
        println("Not implemented, because not needed for tests, yet")
        return flowOf()
    }

    override fun getVolunteerWithSms(): Flow<List<VolunteerWithSms>> {
        println("Not implemented, because not needed for tests, yet")
        return flowOf()
    }

    override fun getVolunteerWithConversations(): Flow<List<VolunteerWithConversations>> {
        println("Not implemented, because not needed for tests, yet")
        return flowOf()
    }

    override fun getVolunteerLocations(): Flow<List<LocationWithVolunteers>> =
        volunteerLocation ?: flowOf()
}