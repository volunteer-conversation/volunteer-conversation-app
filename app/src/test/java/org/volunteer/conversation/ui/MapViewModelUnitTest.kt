package org.volunteer.conversation.ui

import android.location.Address
import android.location.Geocoder
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.nhaarman.mockitokotlin2.any
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.suspendCancellableCoroutine
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnit
import org.volunteer.conversation.BaseUnitTest
import org.volunteer.conversation.persistence.entities.LocationWithVolunteers
import org.volunteer.conversation.persistence.entities.Volunteer
import org.volunteer.conversation.ui.map.MapViewModel
import kotlin.coroutines.resume


@RunWith(JUnit4::class)
class MapViewModelUnitTest : BaseUnitTest() {
    @Rule
    @JvmField
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Rule
    @JvmField
    var mockitoRule = MockitoJUnit.rule()

    @Mock
    lateinit var geocoder: Geocoder
    val volunteerRepository = VolunteerRepositoryMock()
    val mapViewModel = MapViewModel(volunteerRepository)
    @Mock
    lateinit var address: Address

    private val volunteer = Volunteer(
        name = "volunteer1",
        number = "0056 1231344"
    )

    // TODO

    @Test
    fun `GIVEN no locations are parsed WHEN three parsable locations get collected THEN all three locations should be parsed`() =
        runBlocking<Unit> {
            provideThreeParsableLocations()

            mapViewModel.parseVolunteerLocations(geocoder)

            suspendCancellableCoroutine { cont ->
                mapViewModel.locationsWithVolunteers.observeForever {
                    if (it.size == 3) {
                        waitAndMakeSureNoOthersComeMeanwhile()
                        cont.resume(Unit)
                    }

                    if (it.size > 3) {
                        cont.cancel(Error("To many locations with volunteers"))
                    }
                }
            }
        }

    @Test
    fun `GIVEN no locations are parsed WHEN two parsable and one not parsable locations get collected THEN two locations should be added`() =
        runBlocking<Unit> {
            provideTwoParsableOneNotParsableLocations()

            mapViewModel.parseVolunteerLocations(geocoder)

            suspendCancellableCoroutine { cont ->
                mapViewModel.locationsWithVolunteers.observeForever {
                    if (it.size == 2) {
                        waitAndMakeSureNoOthersComeMeanwhile()
                        cont.resume(Unit)
                    }

                    if (it.size > 2) {
                        cont.cancel(Error("To many locations with volunteers"))
                    }
                }
            }
        }

    @Test
    fun `GIVEN three locations are parsed WHEN the same three locations get collected THEN no locations should be parsed`() =
        runBlocking<Unit> {
            provideThreeParsableLocationsAndWaitForProcessing()
            provideThreeParsableLocations()

            mapViewModel.parseVolunteerLocations(geocoder)

            suspendCancellableCoroutine<Unit> { cont ->
                mapViewModel.locationsWithVolunteers.observeForever {
                    if (it.size == 3) {
                        waitAndMakeSureNoOthersComeMeanwhile()
                        cont.resume(Unit)
                    }

                    if (it.size > 3) {
                        cont.cancel(Error("To many locations with volunteers"))
                    }
                }
            }
        }

    private fun provideThreeParsableLocations() {
        addThreeLocationsToDataSource()
        mockAddressLongAndLat()

        `when`(geocoder.getFromLocationName(any(), any())).thenReturn(listOf(address))
    }

    private fun provideTwoParsableOneNotParsableLocations() {
        addThreeLocationsToDataSource()
        mockAddressLongAndLat()

        `when`(geocoder.getFromLocationName(any(), any())).thenReturn(listOf(address))
            .thenReturn(listOf(address)).thenReturn(listOf())
    }

    private suspend fun provideThreeParsableLocationsAndWaitForProcessing() {
        provideThreeParsableLocations()

        mapViewModel.parseVolunteerLocations(geocoder)

        suspendCancellableCoroutine<Unit> { cont ->
            mapViewModel.locationsWithVolunteers.observeForever {
                if (it.size == 3) {
                    cont.resume(Unit)
                }
            }
        }
    }

    private fun addThreeLocationsToDataSource() {
        val location1 =
            LocationWithVolunteers(locationName = "vienna", volunteers = listOf(volunteer))
        val location2 = location1.copy()
        val location3 = location1.copy()
        volunteerRepository.volunteerLocation = flowOf(listOf(location1, location2, location3))
    }

    private fun mockAddressLongAndLat() {
        `when`(address.longitude).thenReturn(0.0)
        `when`(address.latitude).thenReturn(0.0)
    }

    private fun waitAndMakeSureNoOthersComeMeanwhile() {
        Thread.sleep(1000)
    }
}