package org.volunteer.conversation.dao

import android.content.Context
import android.database.sqlite.SQLiteConstraintException
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.first
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.volunteer.conversation.persistence.ConversationDatabase
import org.volunteer.conversation.persistence.daos.ConversationDao
import org.volunteer.conversation.persistence.daos.SmsDao
import org.volunteer.conversation.persistence.daos.VolunteerDao
import org.volunteer.conversation.persistence.entities.Conversation
import org.volunteer.conversation.persistence.entities.Sms
import org.volunteer.conversation.persistence.entities.Subject
import org.volunteer.conversation.persistence.entities.Volunteer

@RunWith(AndroidJUnit4::class)
class SmsDaoUnitTest {
    private lateinit var db: ConversationDatabase
    private lateinit var smsDao: SmsDao
    private lateinit var volunteerDao: VolunteerDao
    private lateinit var conversationDao: ConversationDao

    //TODO: introduce common abstract test data class

    private val volunteer = Volunteer(
        name = "volunteer1",
        number = "0056 1231344"
    )
    private val conversation =
        Conversation(
            subject = Subject.REPORT,
            incident = "flood",
            locationName = "location1",
            info = "info1",
            volunteerId = -1
        )
    private val sms1 =
        Sms(
            text = "text1",
            volunteerId = -1,
            conversationId = -1
        )
    private val sms2 =
        Sms(
            text = "text2",
            volunteerId = -1,
            conversationId = -1
        )


    @Before
    fun beforeEach() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(context, ConversationDatabase::class.java).build()
        smsDao = db.smsDao()
        volunteerDao = db.volunteerDao()
        conversationDao = db.conversationDao()
    }

    @Test
    fun givenASms_WhenPersistTheSms_ThenIdIsReturned() = runBlocking {
        persistVolunteerAndConversationThenSetIdsForSms()

        val id = smsDao.insertSms(sms1)

        assertNotNull(id)
    }

    @Test(expected = SQLiteConstraintException::class)
    fun givenASmsWithoutValidVolunteerId_WhenPersist_ThenSQLiteConstraintException() =
        runBlocking {
            sms1.conversationId = conversationDao.insertConversation(conversation)

            smsDao.insertSms(sms1)
            fail()
        }

    @Test(expected = SQLiteConstraintException::class)
    fun givenASmsWithoutValidConversationId_WhenPersist_ThenSQLiteConstraintException() =
        runBlocking {
            sms1.volunteerId = volunteerDao.insertVolunteer(volunteer)

            smsDao.insertSms(sms1)
            fail()
        }

    @Test
    fun givenASms_WhenPersistTheSms_ThenExactlyThisSmsIsInTheDb() =
        runBlocking {
            persistVolunteerAndConversationThenSetIdsForSms()

            sms1.id = smsDao.insertSms(sms1)

            assertEquals(sms1, smsDao.getSms().first()[0])
        }

    @Test
    fun givenASms_WhenPersistTheSms_ThenCreationTimestampIsGenerated() =
        runBlocking {
            persistVolunteerAndConversationThenSetIdsForSms()

            smsDao.insertSms(sms1)

            assertNotNull(smsDao.getSms().first()[0].createdAtTimestamp)
        }

    @Test
    fun givenTwoSms_WhenPersistBoth_ThenTwoSmsAreInTheDb() = runBlocking {
        persistVolunteerAndConversationThenSetIdsForSms()

        smsDao.insertSms(sms1)
        smsDao.insertSms(sms2)

        assertEquals(2, smsDao.getSms().first().size)
    }

    @InternalCoroutinesApi
    @Test
    fun givenACoroutineThatListensToAllSms_WhenInsertingNewSms_ThenTheCoroutineReceivesTheUpdateSms() =
        runBlocking {
            var sms = emptyList<Sms>()
            GlobalScope.launch {
                smsDao.getSms().collect { v ->
                    sms = v
                }
            }

            persistVolunteerAndConversationThenSetIdsForSms()
            sms1.id = smsDao.insertSms(sms1)
            delay(100)
            assertEquals(listOf(sms1), sms)
            sms2.id = smsDao.insertSms(sms2)
            delay(100)
            assertEquals(listOf(sms1, sms2), sms)
        }

    private suspend fun persistVolunteerAndConversationThenSetIdsForSms() {
        val vid = volunteerDao.insertVolunteer(volunteer)
        volunteer.id = vid
        conversation.volunteerId = vid
        sms1.volunteerId = vid
        sms2.volunteerId = vid

        val cid = conversationDao.insertConversation(conversation)
        conversation.id = cid
        sms1.conversationId = cid
        sms2.conversationId = cid
    }
}
