package org.volunteer.conversation.dao

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.first
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.volunteer.conversation.persistence.ConversationDatabase
import org.volunteer.conversation.persistence.daos.VolunteerDao
import org.volunteer.conversation.persistence.entities.Volunteer

@RunWith(AndroidJUnit4::class)
class VolunteerDaoUnitTest {
    private lateinit var db: ConversationDatabase
    private lateinit var volunteerDao: VolunteerDao

    private val volunteer1 = Volunteer(
        name = "volunteer1",
        number = "0056 1231344"
    )

    private val volunteer2 = Volunteer(
        name = "volunteer2",
        number = "0076 2231354"
    )

    @Before
    fun beforeEach() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(context, ConversationDatabase::class.java).build()
        volunteerDao = db.volunteerDao()
    }

    @Test
    fun givenOneVolunteer_WhenPersistTheVolunteer_ThenIdIsReturned() = runBlocking {
        val id = volunteerDao.insertVolunteer(volunteer1)

        assertNotNull(id)
    }

    @Test
    fun givenOneVolunteer_WhenPersistTheVolunteer_ThenExactlyThisVolunteerIsInTheDb() =
        runBlocking {
            volunteer1.id = volunteerDao.insertVolunteer(volunteer1)

            assertEquals(volunteer1, volunteerDao.getVolunteers().first()[0])
        }

    @Test
    fun givenOneVolunteer_WhenPersistTwice_ThenItIsOnlyOnceInTheDbAndMinus1IsReturnedAtTheSecondTry() =
        runBlocking {
            volunteerDao.insertVolunteer(volunteer1)

            val invalidAttemptId = volunteerDao.insertVolunteer(volunteer1)

            assertEquals(1, volunteerDao.getVolunteers().first().size)
            assertEquals(-1, invalidAttemptId)
        }

    @Test
    fun givenAPersistedVolunteer_WhenFindThisVolunteerByItsNumber_ThenFindExactlyThisVolunteer() =
        runBlocking {
            volunteer1.id = volunteerDao.insertVolunteer(volunteer1)

            val v = volunteerDao.getVolunteerByNumber(volunteer1.number).get()

            assertEquals(volunteer1, v)
        }

    @Test
    fun givenAPersistedVolunteer_WhenFindAVolunteerByAnUnknownNumber_ThenNoVolunteerExists() =
        runBlocking {
            volunteer1.id = volunteerDao.insertVolunteer(volunteer1)

            val v = volunteerDao.getVolunteerByNumber("-123")

            assertFalse(v.isPresent)
        }

    @Test
    fun givenAVolunteer_WhenUpdateVolunteerName_ThenVolunteerHasNewNameWhenReadingFromDb() =
        runBlocking {
            volunteer1.id = volunteerDao.insertVolunteer(volunteer1)

            volunteer1.name = volunteer2.name
            volunteerDao.updateVolunteer(volunteer1)

            assertEquals(
                volunteer2.name,
                volunteerDao.getVolunteers().first()[0].name
            )
        }

    @InternalCoroutinesApi
    @Test
    fun givenACoroutineThatListensToAllVolunteers_WhenInsertingNewVolunteers_ThenTheCorountineReceivesTheUpdateVolunteers() =
        runBlocking {
            var volunteers = emptyList<Volunteer>()
            GlobalScope.launch {
                volunteerDao.getVolunteers().collect { v ->
                    volunteers = v
                }
            }

            volunteer1.id = volunteerDao.insertVolunteer(volunteer1)
            delay(100)
            assertEquals(listOf(volunteer1), volunteers)
            volunteer2.id = volunteerDao.insertVolunteer(volunteer2)
            delay(100)
            assertEquals(listOf(volunteer1, volunteer2), volunteers)
        }
}
