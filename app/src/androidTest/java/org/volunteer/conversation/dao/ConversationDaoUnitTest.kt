package org.volunteer.conversation.dao

import android.content.Context
import android.database.sqlite.SQLiteConstraintException
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.first
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.volunteer.conversation.persistence.ConversationDatabase
import org.volunteer.conversation.persistence.daos.ConversationDao
import org.volunteer.conversation.persistence.daos.VolunteerDao
import org.volunteer.conversation.persistence.entities.Conversation
import org.volunteer.conversation.persistence.entities.ConversationWithVolunteer
import org.volunteer.conversation.persistence.entities.Subject
import org.volunteer.conversation.persistence.entities.Volunteer

@RunWith(AndroidJUnit4::class)
class ConversationDaoUnitTest {
    private lateinit var db: ConversationDatabase
    private lateinit var conversationDao: ConversationDao
    private lateinit var volunteerDao: VolunteerDao

    private val volunteer = Volunteer(
        name = "volunteer1",
        number = "0056 1231344"
    )
    private val conversation1 =
        Conversation(
            subject = Subject.REPORT,
            incident = "flood",
            locationName = "location1",
            info = "info1",
            volunteerId = -1
        )
    private val conversation2 =
        Conversation(
            subject = Subject.REPORT,
            incident = "fire",
            locationName = "location2",
            info = "info2",
            volunteerId = -1
        )

    private val conversationWithVolunteer = ConversationWithVolunteer(conversation1, volunteer)

    @Before
    fun beforeEach() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(context, ConversationDatabase::class.java).build()
        conversationDao = db.conversationDao()
        volunteerDao = db.volunteerDao()
    }

    @Test
    fun givenAConversation_WhenPersistTheConversation_ThenIdIsReturned() = runBlocking {
        persistVolunteerAndSetIdForConversation()

        val id = conversationDao.insertConversation(conversation1)

        assertNotNull(id)
    }

    @Test(expected = SQLiteConstraintException::class)
    fun givenAConversationWithoutValidVolunteerId_WhenPersist_ThenSQLiteConstraintException() =
        runBlocking {
            conversationDao.insertConversation(conversation1)
            fail()
        }

    @Test
    fun givenAConversation_WhenPersistTheConversation_ThenExactlyThisConversationIsInTheDb() =
        runBlocking {
            persistVolunteerAndSetIdForConversation()

            conversation1.id = conversationDao.insertConversation(conversation1)

            assertEquals(conversation1, conversationDao.getConversation().first()[0])
        }

    @Test
    fun givenAConversation_WhenPersistTheConversation_ThenCreationTimestampIsGenerated() =
        runBlocking {
            persistVolunteerAndSetIdForConversation()

            conversationDao.insertConversation(conversation1)

            assertNotNull(conversationDao.getConversation().first()[0].createdAtTimestamp)
        }

    @Test
    fun givenTwoConversations_WhenPersistBoth_ThenTwoConversationsAreInTheDb() = runBlocking {
        persistVolunteerAndSetIdForConversation()

        conversationDao.insertConversation(conversation1)
        conversationDao.insertConversation(conversation2)

        assertEquals(2, conversationDao.getConversation().first().size)
    }


    @Test
    fun givenAConversationWithVolunteer_WhenPersistTheConversation_ThenExactlyThisConversationIncludingTheVolunteerIsInTheDb() =
        runBlocking {
            persistVolunteerAndSetIdForConversation()

            conversation1.id = conversationDao.insertConversation(conversation1)

            assertEquals(
                conversationWithVolunteer,
                conversationDao.getConversationWithVolunteers().first()[0]
            )
        }

    @Test
    fun givenAConversation_WhenUpdateConversationLocation_ThenConversationHasNewLocationWhenReadingFromDb() =
        runBlocking {
            persistVolunteerAndSetIdForConversation()
            conversation1.id = conversationDao.insertConversation(conversation1)

            conversation1.locationName = conversation2.locationName
            conversationDao.updateConversation(conversation1)

            assertEquals(
                conversation2.locationName,
                conversationDao.getConversation().first()[0].locationName
            )
        }


    @InternalCoroutinesApi
    @Test
    fun givenACoroutineThatListensToAllConversations_WhenInsertingNewConversation_ThenTheCorountineReceivesTheUpdateConversations() =
        runBlocking {
            var conversations = emptyList<Conversation>()
            GlobalScope.launch {
                conversationDao.getConversation().collect { v ->
                    conversations = v
                }
            }

            persistVolunteerAndSetIdForConversation()
            conversation1.id = conversationDao.insertConversation(conversation1)
            delay(100)
            assertEquals(listOf(conversation1), conversations)
            conversation2.id = conversationDao.insertConversation(conversation2)
            delay(100)
            assertEquals(listOf(conversation1, conversation2), conversations)
        }

    private suspend fun persistVolunteerAndSetIdForConversation() {
        val vid = volunteerDao.insertVolunteer(volunteer)
        volunteer.id = vid
        conversation1.volunteerId = vid
        conversation2.volunteerId = vid
    }
}
