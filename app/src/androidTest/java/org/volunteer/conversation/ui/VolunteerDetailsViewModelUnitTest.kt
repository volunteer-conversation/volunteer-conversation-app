package org.volunteer.conversation.ui

import android.content.Context
import android.net.Uri
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.matcher.ViewMatchers.assertThat
import androidx.test.ext.junit.runners.AndroidJUnit4
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.runBlocking
import org.hamcrest.CoreMatchers.containsString
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.volunteer.conversation.persistence.entities.Volunteer
import org.volunteer.conversation.ui.volunteer.details.VolunteerDetailsViewModel
import java.io.BufferedReader
import java.io.InputStreamReader

@RunWith(AndroidJUnit4::class)
class VolunteerDetailsViewModelUnitTest {
    private val context = ApplicationProvider.getApplicationContext<Context>()
    private lateinit var volunteerDetailsViewModel: VolunteerDetailsViewModel

    private val volunteer = Volunteer(
        name = "volunteer1",
        number = "0056 1231344"
    )

    @Before
    fun beforeEach() = runBlocking {
        volunteerDetailsViewModel = VolunteerDetailsViewModel()
    }

    @Test
    fun givenConversations_whenExported_thenShouldCreateVCF() = runBlocking {
        volunteerDetailsViewModel.shareVolunteer(
            context,
            volunteer
        ) { _: String, _: String, uri: Uri ->
            val fileContent = getFileContent(uri)
            assertThat(fileContent, containsString(volunteer.name))
            assertThat(fileContent, containsString(volunteer.number))
        }
    }

    @Test
    fun givenConversations_whenExported_thenTitleShouldBeUseful() = runBlocking {
        volunteerDetailsViewModel.shareVolunteer(
            context,
            volunteer
        ) { title: String, _: String, _: Uri ->
            assertThat(title, containsString("Share volunteer " + volunteer.name))
        }
    }

    @Test
    fun givenConversations_whenExported_thenFileNameShouldContainVolunteer() =
        runBlocking<Unit> {
            volunteerDetailsViewModel.shareVolunteer(
                context,
                volunteer
            ) { _: String, fileName: String, fileUri: Uri ->
                assertThat(fileName, containsString(volunteer.name))
                assertThat(fileUri.path, containsString(".vcf"))
            }
        }

    @Test
    fun givenConversations_whenExported_thenShouldSetUriAuthority() = runBlocking {
        volunteerDetailsViewModel.shareVolunteer(
            context,
            volunteer
        ) { _: String, _: String, uri: Uri ->
            assertEquals(uri.authority, "org.volunteer.conversation.fileprovider")
        }
    }

    private fun getFileContent(uri: Uri): String {
        var fileContent = ""
        val stream = context.contentResolver.openInputStream(uri)
        val r = BufferedReader(InputStreamReader(stream!!))
        var csvLine: String?
        while (r.readLine().also { csvLine = it } != null) {
            fileContent += csvLine
        }
        return fileContent
    }
}