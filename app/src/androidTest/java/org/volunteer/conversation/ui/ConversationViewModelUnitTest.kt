package org.volunteer.conversation.ui

import android.content.Context
import android.net.Uri
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.matcher.ViewMatchers.assertThat
import androidx.test.ext.junit.runners.AndroidJUnit4
import kotlinx.coroutines.runBlocking
import org.hamcrest.CoreMatchers.containsString
import org.hamcrest.CoreMatchers.not
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.volunteer.conversation.persistence.ConversationDatabase
import org.volunteer.conversation.persistence.ConversationRepository
import org.volunteer.conversation.persistence.entities.Conversation
import org.volunteer.conversation.persistence.entities.Subject
import org.volunteer.conversation.persistence.entities.Volunteer
import org.volunteer.conversation.ui.conversation.ConversationViewModel
import java.io.BufferedReader
import java.io.InputStreamReader

@RunWith(AndroidJUnit4::class)
class ConversationViewModelUnitTest {
    private val context = ApplicationProvider.getApplicationContext<Context>()
    private lateinit var conversationViewModel: ConversationViewModel

    private val volunteer = Volunteer(
        name = "volunteer1",
        number = "0056 1231344"
    )
    private val conversation =
        Conversation(
            subject = Subject.REPORT,
            incident = "flood",
            locationName = "location1",
            info = "info1",
            volunteerId = -1
        )

    @Before
    fun beforeEach() = runBlocking {
        val db = Room.inMemoryDatabaseBuilder(context, ConversationDatabase::class.java).build()
        val conversationRepository = ConversationRepository(db)
        conversationRepository.insertConversation(volunteer, conversation)
        conversationViewModel = ConversationViewModel(conversationRepository)
    }

    @Test
    fun givenConversations_whenExported_thenShouldCreateCSV() = runBlocking {
        conversationViewModel.exportConversations(context) { _: String, _: String, uri: Uri ->
            val fileContent = getFileContent(uri)
            assertThat(fileContent, containsString("Volunteer"))

            throw Error("Help me @pmoser, why are test db data not read?")
        }
    }

    @Test
    fun givenConversations_whenExported_thenTitleShouldBeUseful() = runBlocking {
        conversationViewModel.exportConversations(context) { title: String, _: String, _: Uri ->
            assertThat(title, containsString("Conversation Export"))
        }
    }

    @Test
    fun givenConversations_whenExported_thenFileNameShouldNotContainSpaceBeLowerSpaceAndBeUseful() =
        runBlocking {
            conversationViewModel.exportConversations(context) { _: String, fileName: String, _: Uri ->
                fileName.toCharArray().forEach {
                    assertTrue(
                        it.isLowerCase() || it.isDigit() || it == "_".toCharArray()
                            .first() || it == ".".toCharArray().first()
                    )
                }
                assertThat(fileName, not(containsString(" ")))
                assertThat(fileName, containsString("conversation"))
                assertThat(fileName, containsString("export"))
            }
        }

    @Test
    fun givenConversations_whenExported_thenShouldSetUriAuthority() = runBlocking {
        conversationViewModel.exportConversations(context) { _: String, _: String, uri: Uri ->
            assertEquals(uri.authority, "org.volunteer.conversation.fileprovider")
        }
    }

    private fun getFileContent(uri: Uri): String {
        var fileContent = ""
        val stream = context.contentResolver.openInputStream(uri)
        val r = BufferedReader(InputStreamReader(stream!!))
        var csvLine: String?
        while (r.readLine().also { csvLine = it } != null) {
            fileContent += csvLine
        }
        return fileContent
    }
}