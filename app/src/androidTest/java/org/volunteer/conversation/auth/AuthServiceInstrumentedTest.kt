package org.volunteer.conversation.auth

import android.content.Context
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import kotlinx.coroutines.runBlocking
import net.openid.appauth.AuthorizationException
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class AuthServiceInstrumentedTest {
    companion object {
        const val SHARED_AUTH_PREFERENCES_NAME = "auth"
    }

    private val appContext = InstrumentationRegistry.getInstrumentation().targetContext
    private val googleAuthServiceProviderMock = GoogleAuthServiceProviderMock()
    private val authSecretStore = AuthSecretStore()
    private val authService = AuthService(authSecretStore, googleAuthServiceProviderMock)
    private val sharedAuthPreferences =
        appContext.getSharedPreferences(SHARED_AUTH_PREFERENCES_NAME, Context.MODE_PRIVATE)

    @Before
    fun setUp() {
        clearSharedAuthPreferences()
    }

    @Test
    fun givenAContextWhenNoRefreshTokenIsAvailableThenItShouldAuthorizeUsingGoogle() = runBlocking {
        authService.authorize(appContext)

        assertOnlyGoogleLoginWasCalled()
    }

    @Test
    fun givenAContextWhenRefreshTokenIsAvailableThenAuthorizeUsingRefreshTokenFirstAndTryGoogleOnFail() =
        runBlocking {
            storeRefreshToken()
            googleAuthServiceProviderMock.performTokenRequestException =
                AuthorizationException(0, 0, null, null, null, null)

            authService.authorize(appContext)

            assertTokenRequestAndThenGoogleRequestWasCalled()
        }

    @Test
    fun givenAContextWhenRefreshTokenIsAvailableThenAuthorizeUsingRefreshToken() =
        runBlocking {
            storeRefreshToken()

            authService.authorize(appContext)

            assertTokenRequestWasCalled()
        }

    private fun clearSharedAuthPreferences() = sharedAuthPreferences.edit().clear().commit()

    private fun assertOnlyGoogleLoginWasCalled() {
        assertEquals(googleAuthServiceProviderMock.performTokenRequestCount, 0)
        assertEquals(googleAuthServiceProviderMock.performAuthorizationRequestCount, 1)
    }

    private fun assertTokenRequestAndThenGoogleRequestWasCalled() {
        assertEquals(googleAuthServiceProviderMock.performTokenRequestCount, 1)
        assertEquals(googleAuthServiceProviderMock.performAuthorizationRequestCount, 1)
    }

    private fun assertTokenRequestWasCalled() {
        assertEquals(googleAuthServiceProviderMock.performTokenRequestCount, 1)
        assertEquals(googleAuthServiceProviderMock.performAuthorizationRequestCount, 0)
    }

    private fun storeRefreshToken() {
        authSecretStore.store(appContext, "a refresh token")
    }
}