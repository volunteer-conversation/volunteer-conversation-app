package org.volunteer.conversation.auth

import android.app.PendingIntent
import android.content.Context
import net.openid.appauth.*

class GoogleAuthServiceProviderMock : AuthServiceProvider {
    var performTokenRequestCount = 0
    var performAuthorizationRequestCount = 0
    private val mockedTokenResponse =
        "{\"request\":{\"configuration\":{\"authorizationEndpoint\":\"https:\\/\\/accounts.google.com\\/o\\/oauth2\\/v2\\/auth\",\"tokenEndpoint\":\"https:\\/\\/www.googleapis.com\\/oauth2\\/v4\\/token\"},\"clientId\":\"963934995792-0r8ba6mqql76tvgntmkcorhg5d7u5b6v.apps.googleusercontent.com\",\"grantType\":\"authorization_code\",\"redirectUri\":\"org.volunteer.conversation:\\/\",\"authorizationCode\":\"4\\/0AEVEW1O4tQqbCffOoij1OH9wynu7wnTTzZU9vR3fi2wGjt_951QPk64sQZNceApGTp9lQSpdTp-z0eHvs9fLL4\",\"additionalParameters\":{}},\"token_type\":\"Bearer\",\"access_token\":\"ya29.a0AfH6SMANYfkDaQOJNnvYsQ2jvE1iJHHer2DxLt6dcWkx0nRYhcHoWK7In_isThaIq0Y1736CXHSQx7pikpF4rM6pRzwLzNavBUhSHNDtD3edVRHPiSiHluWFN3uPpHPPy_aZ7tbxf53m4DFeg7779QYD93wHJp5hRo8\",\"expires_at\":1590145590568,\"id_token\":\"eyJhbGciOiJSUzI1NiIsImtpZCI6Ijk2MGE3ZThlODM0MWVkNzUyZjEyYjE4NmZhMTI5NzMxZmUwYjA0YzAiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL2FjY291bnRzLmdvb2dsZS5jb20iLCJhenAiOiI5NjM5MzQ5OTU3OTItMHI4YmE2bXFxbDc2dHZnbnRta2NvcmhnNWQ3dTViNnYuYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJhdWQiOiI5NjM5MzQ5OTU3OTItMHI4YmE2bXFxbDc2dHZnbnRta2NvcmhnNWQ3dTViNnYuYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJzdWIiOiIxMTA3MTkyODY3OTA4MTk5NTYyODAiLCJhdF9oYXNoIjoiN3dzcHNMd2Y5Uld6eGlQZF9VUTllUSIsIm5hbWUiOiJNYXJsb24gQWxhZ29kYSIsInBpY3R1cmUiOiJodHRwczovL2xoMy5nb29nbGV1c2VyY29udGVudC5jb20vYS0vQU9oMTRHaTF4N3ZxblZRTnBjYUo0YnpOUXU5X05JZG1OYjJtZ1RRVEhaTVNfWDg9czk2LWMiLCJnaXZlbl9uYW1lIjoiTWFybG9uIiwiZmFtaWx5X25hbWUiOiJBbGFnb2RhIiwibG9jYWxlIjoiZGUiLCJpYXQiOjE1OTAxNDE5OTIsImV4cCI6MTU5MDE0NTU5Mn0.Miy6YNQOZMT2AeNYGtIlPc5txyTEGMv5zxP0_gdCaVdMCGIruCcOQhyTufkiJLneeyBqlfAGYl-BRBEekHalWrZ7FsIs8juRxl_3ZUaUCCAifjRuRdcIBilbklOtwSPTBNc7EjvmEOqSibETsvFovEj059klqPeY3ZNiFQEqt_OCjVCRQPTbywM-I-hpnBZZEtPbAk8zFZUuhcEaICtYzHEvoDSluQCNE_mr126m4opj8R1Ziieptz43SXTZuXcB6kYE2qAfnhyMxWP34f4QPmaVocGkd7zVhqVz2NW1lGwqbqu4Ss3mjCCKrNq756ZboSwzUAp16BrH7e0h4AqSGQ\",\"refresh_token\":\"1\\/\\/098X7LyYOe6gkCgYIARAAGAkSNwF-L9IruTEutcdT01twiBzqaCCb0xf4f5H5PqlMEx8iPZoW7Q3x8o93EFDsNjvf93DRSaHhqEs\",\"scope\":\"https:\\/\\/www.googleapis.com\\/auth\\/cloud-platform https:\\/\\/www.googleapis.com\\/auth\\/dialogflow https:\\/\\/www.googleapis.com\\/auth\\/userinfo.profile\",\"additionalParameters\":{}}"
    var performTokenRequestException: AuthorizationException? = null

    override fun performTokenRequest(
        context: Context,
        request: TokenRequest,
        callback: AuthorizationService.TokenResponseCallback
    ) {
        performTokenRequestCount++
        val tokenResponse =
            TokenResponse.Builder(request).fromResponseJsonString(mockedTokenResponse).build()
        callback.onTokenRequestCompleted(tokenResponse, performTokenRequestException)
    }

    override fun performAuthorizationRequest(
        context: Context,
        request: AuthorizationRequest,
        completedIntent: PendingIntent
    ) {
        performAuthorizationRequestCount++
    }
}