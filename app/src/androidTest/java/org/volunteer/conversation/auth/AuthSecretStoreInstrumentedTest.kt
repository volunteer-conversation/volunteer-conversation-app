package org.volunteer.conversation.auth

import android.content.Context
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class AuthSecretStoreInstrumentedTest {
    companion object {
        const val SHARED_AUTH_PREFERENCES_NAME = "auth"
        const val SHARED_AUTH_PREFERENCES_KEY = "refresh_token"
    }

    private val authSecretStore = AuthSecretStore()
    private val appContext = InstrumentationRegistry.getInstrumentation().targetContext
    private val sharedAuthPreferences =
        appContext.getSharedPreferences(SHARED_AUTH_PREFERENCES_NAME, Context.MODE_PRIVATE)

    @Before
    fun before() {
        clearSharedAuthPreferences()
    }

    @Test
    fun GIVEN_a_refresh_token_WHEN_it_is_passed_to_the_AuthSecretStore_THEN_it_gets_stored_in_shared_references() {
        assertFalse(doesRefreshTokenExist())

        authSecretStore.store(appContext, "a secret")

        assertTrue(doesRefreshTokenExist())
    }

    @Test
    fun GIVEN_a_refresh_token_WHEN_it_is_passed_to_the_AuthSecretStore_THEN_it_gets_encrypted() {
        val unencryptedSecret = "a secret"

        authSecretStore.store(appContext, unencryptedSecret)

        val encryptedSecret = getRefreshTokenFromSharedAuthPreferences()
        assertTrue(encryptedSecret != null)
        assertTrue(encryptedSecret != unencryptedSecret)
    }

    @Test
    fun GIVEN_no_refresh_token_is_stored_WHEN_requested_THEN_it_can_not_be_retrieved() {
        val refreshToken = authSecretStore.retrieveRefreshToken(appContext)

        assertTrue(refreshToken == null)
    }

    @Test
    fun GIVEN_a_refresh_token_is_stored_WHEN_requested_THEN_it_can_be_retrieved() {
        authSecretStore.store(appContext, "a secret")

        val refreshToken = authSecretStore.retrieveRefreshToken(appContext)
        assertTrue(refreshToken != null)
    }

    @Test
    fun GIVEN_a_refresh_token_is_stored_WHEN_requested_THEN_it_get_s_decrypted() {
        val unencryptedSecret = "a secret"

        authSecretStore.store(appContext, unencryptedSecret)
        val refreshToken = authSecretStore.retrieveRefreshToken(appContext)

        assertEquals(unencryptedSecret, refreshToken)
    }

    private fun getRefreshTokenFromSharedAuthPreferences() =
        sharedAuthPreferences.getString(SHARED_AUTH_PREFERENCES_KEY, null)

    private fun clearSharedAuthPreferences() = sharedAuthPreferences.edit().clear().commit()

    private fun doesRefreshTokenExist(): Boolean {
        val refreshTokenBeforeStoring = getRefreshTokenFromSharedAuthPreferences()
        return !refreshTokenBeforeStoring.isNullOrEmpty()
    }
}