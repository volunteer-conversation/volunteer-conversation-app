package org.volunteer.conversation.di

import dagger.Module
import org.volunteer.conversation.ui.conversation.di.ConversationComponent
import org.volunteer.conversation.ui.map.di.MapComponent
import org.volunteer.conversation.ui.volunteer.di.VolunteerComponent

@Module(
    subcomponents = [
        MapComponent::class,
        ConversationComponent::class,
        VolunteerComponent::class
    ]
)
object SubcomponentsModule