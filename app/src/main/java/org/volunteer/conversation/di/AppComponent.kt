package org.volunteer.conversation.di

import android.app.Application
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import org.volunteer.conversation.VolunteerConversationApp
import org.volunteer.conversation.auth.di.AuthModule
import org.volunteer.conversation.chatbot.di.ChatbotModule
import org.volunteer.conversation.di.viewmodelfactory.ViewModelBuilderModule
import org.volunteer.conversation.persistence.di.PersistenceModule
import org.volunteer.conversation.sms.di.MessageModule
import org.volunteer.conversation.ui.MainActivity
import org.volunteer.conversation.ui.conversation.di.ConversationComponent
import org.volunteer.conversation.ui.map.di.MapComponent
import org.volunteer.conversation.ui.volunteer.di.VolunteerComponent
import javax.inject.Singleton


@Singleton
@Component(
    modules = [
        SubcomponentsModule::class,
        ViewModelBuilderModule::class,
        MessageModule::class,
        ChatbotModule::class,
        AuthModule::class,
        AndroidSupportInjectionModule::class,
        PersistenceModule::class
    ]
)
interface AppComponent : AndroidInjector<VolunteerConversationApp> {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder
        fun build(): AppComponent
    }

    fun inject(mainActivity: MainActivity)

    val mapComponentBuilder: MapComponent.Builder
    val conversationComponent: ConversationComponent.Builder
    val volunteerComponent: VolunteerComponent.Builder
}