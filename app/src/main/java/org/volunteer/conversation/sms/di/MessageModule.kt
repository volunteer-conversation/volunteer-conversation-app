package org.volunteer.conversation.sms.di

import dagger.Binds
import dagger.Module
import org.volunteer.conversation.sms.MessageListener
import org.volunteer.conversation.sms.MessageProcessor

@Module
abstract class MessageModule {
    @Binds
    abstract fun provideMessageListener(messageProcessor: MessageProcessor): MessageListener
}