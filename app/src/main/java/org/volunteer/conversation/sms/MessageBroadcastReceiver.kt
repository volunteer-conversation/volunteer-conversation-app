package org.volunteer.conversation.sms

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.provider.Telephony.Sms.Intents.SMS_RECEIVED_ACTION
import android.provider.Telephony.Sms.Intents.getMessagesFromIntent
import org.volunteer.conversation.sms.dtos.IncomingMessage
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MessageBroadcastReceiver @Inject constructor(private val messageListener: MessageListener) :
    BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        if (SMS_RECEIVED_ACTION == intent.action) {
            for (smsMessage in getMessagesFromIntent(intent)) {
                messageListener.sendMessage(
                    IncomingMessage(
                        smsMessage.displayOriginatingAddress,
                        smsMessage.displayMessageBody
                    )
                )
            }
        }
    }
}