package org.volunteer.conversation.sms

import android.telephony.SmsManager
import javax.inject.Inject

class CustomSmsManager @Inject constructor() {
    val smsManager: SmsManager = SmsManager.getDefault() as SmsManager

    fun sendTextMessage(
        phoneNumber: String,
        text: String
    ) {
        var shortText = text
        while(shortText.isNotEmpty()) {
            smsManager.sendTextMessage(phoneNumber, null, shortText.take(160), null, null)
            shortText = shortText.drop(160)
        }
    }
}