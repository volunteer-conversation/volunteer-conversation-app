package org.volunteer.conversation.sms

import org.volunteer.conversation.sms.dtos.IncomingMessage

interface MessageListener {
    fun sendMessage(incomingMessage: IncomingMessage)
}