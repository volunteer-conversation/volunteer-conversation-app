package org.volunteer.conversation.sms.dtos

data class OutgoingMessage(val phoneNumber: String, val text: String)