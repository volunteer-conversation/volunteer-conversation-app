package org.volunteer.conversation.sms

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.launch
import org.volunteer.conversation.sms.dtos.IncomingMessage
import org.volunteer.conversation.util.Log
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MessageProcessor @Inject constructor() :
    MessageListener {

    private val messageChannel = Channel<IncomingMessage>()

    override fun sendMessage(incomingMessage: IncomingMessage) {
        val text = incomingMessage.text
        Log.d(this::class.simpleName, text)
        GlobalScope.launch {
            messageChannel.send(incomingMessage)
        }
    }

    suspend fun receive() = messageChannel.receive()
}