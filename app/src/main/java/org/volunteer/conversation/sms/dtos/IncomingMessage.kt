package org.volunteer.conversation.sms.dtos

data class IncomingMessage(val phoneNumber: String, val text: String)