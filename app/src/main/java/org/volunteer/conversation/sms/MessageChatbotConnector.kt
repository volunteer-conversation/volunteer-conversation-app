package org.volunteer.conversation.sms

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.volunteer.conversation.chatbot.ChatbotQueryException
import org.volunteer.conversation.chatbot.ChatbotService
import org.volunteer.conversation.chatbot.dtos.QueryResult
import org.volunteer.conversation.persistence.ConversationRepository
import org.volunteer.conversation.persistence.SmsRepository
import org.volunteer.conversation.persistence.entities.Conversation
import org.volunteer.conversation.persistence.entities.Sms
import org.volunteer.conversation.persistence.entities.Subject
import org.volunteer.conversation.persistence.entities.Volunteer
import org.volunteer.conversation.sms.dtos.IncomingMessage
import org.volunteer.conversation.sms.dtos.OutgoingMessage
import org.volunteer.conversation.util.Log
import javax.inject.Inject

class MessageChatbotConnector @Inject constructor(
    private val messageProcessor: MessageProcessor,
    private val chatbotService: ChatbotService,
    private val customSmsManager: CustomSmsManager,
    private val conversationRepository: ConversationRepository,
    private val smsRepository: SmsRepository
) {
    fun listenAndRespondToSms() {
        GlobalScope.launch {
            while (true) {
                val receivedMessage = messageProcessor.receive()

                smsRepository.insertSms(
                    volunteer = Volunteer(number = receivedMessage.phoneNumber, name = null),
                    sms = Sms(text = receivedMessage.text)
                )

                var chatbotQueryResult: QueryResult
                try {
                    chatbotQueryResult = chatbotService.getQueryResult(receivedMessage)
                } catch (chatbotQueryException: ChatbotQueryException) {
                    // TODO send to failed notification queue, check for handle-able failure scenarios: Google Token not valid anymore, ... ?
                    Log.d(
                        this::class.simpleName,
                        "Send " + chatbotQueryException.incomingMessage + " to failed notification queue"
                    )
                    continue
                }

                if (chatbotService.isFullConversation(chatbotQueryResult)) {
                    insertConversation(chatbotQueryResult, receivedMessage)
                } else if (chatbotService.isLocationUpdate(chatbotQueryResult)) {
                    val volunteer = getVolunteer(chatbotQueryResult, receivedMessage)
                    conversationRepository.insertLocationForLastConversationIfVolunteerExists(
                        volunteer,
                        chatbotQueryResult.parameters.location
                    )
                }

                sendSms(
                    OutgoingMessage(
                        receivedMessage.phoneNumber,
                        chatbotQueryResult.fulfillmentText
                    )
                )
            }
        }
    }

    private fun sendSms(outgoingMessage: OutgoingMessage) {
        customSmsManager.sendTextMessage(outgoingMessage.phoneNumber, outgoingMessage.text)
    }

    private suspend fun insertConversation(
        chatbotQueryResult: QueryResult,
        receivedMessage: IncomingMessage
    ) {
        val volunteer = getVolunteer(chatbotQueryResult, receivedMessage)

        val conversation = Conversation(
            //TODO: find out correct subject -> depends on dialogflow response
            subject = Subject.REPORT,
            incident = chatbotQueryResult.parameters.report,
            locationName = chatbotQueryResult.parameters.location.city,
            info = chatbotQueryResult.parameters.unknownProperties.toString()
        )

        conversationRepository.insertConversation(volunteer, conversation)
    }

    private fun getVolunteer(chatbotQueryResult: QueryResult, receivedMessage: IncomingMessage) =
        Volunteer(
            name = chatbotQueryResult.parameters.name,
            number = receivedMessage.phoneNumber
        )
}