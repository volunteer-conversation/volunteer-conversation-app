package org.volunteer.conversation.chatbot.dtos

import com.google.gson.*
import java.lang.reflect.Type

class QueryResultParametersDeserializer : JsonDeserializer<QueryResultParameters> {
    //TODO: change to update names
    companion object {
        const val LOCATION_MEMBER_NAME = "location"
        const val NAME_MEMBER_NAME = "name"
        const val REPORT_MEMBER_NAME = "report"
    }

    override fun deserialize(
        json: JsonElement?,
        typeOfT: Type?,
        context: JsonDeserializationContext?
    ): QueryResultParameters {
        val jsonObject: JsonObject = json?.asJsonObject ?: JsonObject()

        val name = removeMember(jsonObject, NAME_MEMBER_NAME)
        val report = removeMember(jsonObject, REPORT_MEMBER_NAME)
        val location = removeLocationStringOrObject(jsonObject)

        return QueryResultParameters(
            location = location,
            name = name,
            report = report,
            unknownProperties = jsonObject
        )
    }

    private fun removeMember(
        jsonObject: JsonObject,
        memberName: String,
        default: String = ""
    ): String {
        val value = if (jsonObject.has(memberName)) {
            jsonObject.get(memberName).asString
        } else {
            default
        }

        jsonObject.remove(memberName)
        return value
    }

    private fun removeLocationStringOrObject(jsonObject: JsonObject): QueryResultLocation {
        var location = QueryResultLocation("")

        if (!jsonObject.has(LOCATION_MEMBER_NAME)) {
            return location
        }

        val locationAsStringOrObject = jsonObject.get(LOCATION_MEMBER_NAME)

        if (locationAsStringOrObject.isJsonObject) {
            location = Gson().fromJson(
                jsonObject.get(LOCATION_MEMBER_NAME),
                QueryResultLocation::class.java
            )
        } else if (locationAsStringOrObject.isJsonPrimitive) {
            location = QueryResultLocation(locationAsStringOrObject.asString)
        }

        jsonObject.remove(LOCATION_MEMBER_NAME)

        return location
    }
}