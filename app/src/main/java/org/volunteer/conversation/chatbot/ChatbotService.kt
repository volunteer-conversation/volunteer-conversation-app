package org.volunteer.conversation.chatbot

import org.volunteer.conversation.chatbot.dtos.QueryResult
import org.volunteer.conversation.chatbot.dtos.QueryResultLocation
import org.volunteer.conversation.chatbot.dtos.QueryResultParameters
import org.volunteer.conversation.sms.dtos.IncomingMessage
import retrofit2.HttpException
import javax.inject.Inject

class ChatbotService @Inject constructor(val chatbotRepository: ChatbotRepository) {
    @Throws(ChatbotQueryException::class)
    suspend fun getQueryResult(incomingMessage: IncomingMessage): QueryResult {
        try {
            return chatbotRepository.getAnswer(incomingMessage.phoneNumber, incomingMessage.text)
        } catch (e: HttpException) {
            throw ChatbotQueryException(incomingMessage)
        }
    }

    fun isFullConversation(queryResult: QueryResult) =
        hasMinimumCommonParameters(queryResult.parameters) && queryResult.allRequiredParamsPresent

    fun isLocationUpdate(queryResult: QueryResult) =
        !isFullConversation(queryResult) && locationIsSet(queryResult.parameters.location)

    private fun hasMinimumCommonParameters(parameters: QueryResultParameters) =
        parameters.report.isNotEmpty()

    private fun locationIsSet(location: QueryResultLocation) = location.city.isNotBlank()
}