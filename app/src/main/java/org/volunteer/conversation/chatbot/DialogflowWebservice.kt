package org.volunteer.conversation.chatbot

import org.volunteer.conversation.chatbot.dtos.DetectIntentRequest
import org.volunteer.conversation.chatbot.dtos.DetectIntentResponse
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.POST
import retrofit2.http.Path

interface DialogflowWebservice {
    @POST("/v2/projects/{dialogflow_chatbot_id}/agent/sessions/{session}:detectIntent")
    suspend fun detectIntent(
        @Header("Authorization") token: String,
        @Path("session") session: String,
        @Path("dialogflow_chatbot_id") dialogflowChatbotId: String,
        @Body detectIntentRequest: DetectIntentRequest
    ): DetectIntentResponse
}