package org.volunteer.conversation.chatbot.dtos

data class QueryResultLocation(val city: String = "")