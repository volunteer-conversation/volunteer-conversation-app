package org.volunteer.conversation.chatbot.dtos

// Google API documentation: https://cloud.google.com/dialogflow/docs/reference/rest/v2/DetectIntentResponse

data class TextInput(val text: String, val languageCode: String = "en")