package org.volunteer.conversation.chatbot.dtos

import com.google.gson.JsonElement
import com.google.gson.JsonObject

data class QueryResultParameters(
    val location: QueryResultLocation = QueryResultLocation(),
    val name: String = "",
    val report: String = "",
    val unknownProperties: JsonElement = JsonObject()
)