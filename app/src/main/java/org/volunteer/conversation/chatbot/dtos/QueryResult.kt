package org.volunteer.conversation.chatbot.dtos

data class QueryResult(
    val fulfillmentText: String = "No answer available",
    val parameters: QueryResultParameters = QueryResultParameters(),
    val allRequiredParamsPresent: Boolean = false
)