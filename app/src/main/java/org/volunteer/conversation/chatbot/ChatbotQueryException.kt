package org.volunteer.conversation.chatbot

import org.volunteer.conversation.sms.dtos.IncomingMessage

class ChatbotQueryException(val incomingMessage: IncomingMessage) :
    Exception("Could not process message with text " + incomingMessage.text + " at chatbot")