package org.volunteer.conversation.chatbot

import org.volunteer.conversation.R
import org.volunteer.conversation.VolunteerConversationApp
import org.volunteer.conversation.auth.AuthService
import org.volunteer.conversation.chatbot.dtos.DetectIntentRequest
import org.volunteer.conversation.chatbot.dtos.QueryInput
import org.volunteer.conversation.chatbot.dtos.QueryResult
import org.volunteer.conversation.chatbot.dtos.TextInput
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ChatbotRepository @Inject constructor(
    val dialogflowWebservice: DialogflowWebservice,
    private val authService: AuthService
) {
    suspend fun getAnswer(session: String, text: String): QueryResult {
        val detectIntentRequest = DetectIntentRequest(QueryInput(TextInput(text)))
        return dialogflowWebservice.detectIntent(
            "Bearer " + authService.accessToken,
            session,
            VolunteerConversationApp.getResources()?.getString(R.string.dialogflow_chatbot_id)
                ?: "",
            detectIntentRequest
        ).queryResult
    }
}