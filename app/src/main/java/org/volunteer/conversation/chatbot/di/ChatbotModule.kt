package org.volunteer.conversation.chatbot.di

import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.volunteer.conversation.chatbot.DialogflowWebservice
import org.volunteer.conversation.chatbot.dtos.QueryResultParameters
import org.volunteer.conversation.chatbot.dtos.QueryResultParametersDeserializer
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


@Module
class ChatbotModule {
    companion object {
        private const val DIALOGFLOW_API_ENDPOINT = "https://dialogflow.googleapis.com"
    }

    @Provides
    fun provideDialogflowWebservice(): DialogflowWebservice {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        val client =
            OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .build()

        val gson = GsonBuilder()
            .registerTypeAdapter(
                QueryResultParameters::class.java,
                QueryResultParametersDeserializer()
            )
            .create()

        return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create(gson))
            .baseUrl(DIALOGFLOW_API_ENDPOINT)
            .client(client)
            .build()
            .create(DialogflowWebservice::class.java)
    }
}