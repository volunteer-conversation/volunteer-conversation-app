package org.volunteer.conversation.auth.di

import dagger.Binds
import dagger.Module
import org.volunteer.conversation.auth.AuthServiceProvider
import org.volunteer.conversation.auth.GoogleAuthServiceProvider

@Module
abstract class AuthModule {
    @Binds
    abstract fun inject(googleAuthServiceProvider: GoogleAuthServiceProvider): AuthServiceProvider
}