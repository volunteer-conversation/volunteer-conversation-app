package org.volunteer.conversation.auth

import android.app.PendingIntent
import android.content.Context
import net.openid.appauth.AuthorizationRequest
import net.openid.appauth.AuthorizationService
import net.openid.appauth.TokenRequest
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GoogleAuthServiceProvider @Inject constructor() : AuthServiceProvider {
    override fun performTokenRequest(
        activityContext: Context,
        request: TokenRequest,
        callback: AuthorizationService.TokenResponseCallback
    ) {
        val authorizationService = AuthorizationService(activityContext)
        authorizationService.performTokenRequest(request, callback)
        authorizationService.dispose()
    }

    override fun performAuthorizationRequest(
        context: Context,
        request: AuthorizationRequest,
        completedIntent: PendingIntent
    ) {
        val authorizationService = AuthorizationService(context)
        authorizationService.performAuthorizationRequest(request, completedIntent)
        authorizationService.dispose()
    }
}