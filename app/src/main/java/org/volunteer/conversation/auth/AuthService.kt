package org.volunteer.conversation.auth

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.net.Uri
import net.openid.appauth.*
import org.volunteer.conversation.ui.MainActivity
import javax.inject.Inject
import javax.inject.Singleton
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine


@Singleton
class AuthService @Inject constructor(
    private val authSecretStore: AuthSecretStore,
    private val authServiceProvider: AuthServiceProvider
) {
    companion object {
        const val CLIENT_ID =
            "963934995792-0r8ba6mqql76tvgntmkcorhg5d7u5b6v.apps.googleusercontent.com"
        val REDIRECT_URI_STRING = "org.volunteer.conversation:/"

        const val PROFILE_SCOPE = "profile"
        const val GCP_SCOPE = "https://www.googleapis.com/auth/cloud-platform"
        const val DF_SCOPE = "https://www.googleapis.com/auth/dialogflow"

        const val AUTH_ENDPOINT = "https://accounts.google.com/o/oauth2/v2/auth"
        const val TOKEN_ENDPOINT = "https://www.googleapis.com/oauth2/v4/token"
    }

    private var authState: AuthState = AuthState()

    var accessToken = authState.accessToken

    suspend fun authorize(context: Context) {
        val storedRefreshToken = authSecretStore.retrieveRefreshToken(context)

        if (storedRefreshToken != null) {
            try {
                authorizeUsingRefreshToken(context, storedRefreshToken)
            } catch (e: Throwable) {
                authorizeUsingGoogleLogin(context)
            }
        } else {
            authorizeUsingGoogleLogin(context)
        }
    }

    fun isAuthorized() = this.authState.isAuthorized

    private suspend fun authorizeUsingRefreshToken(context: Context, refreshToken: String) {
        val authorizationServiceConfiguration = AuthorizationServiceConfiguration(
            Uri.parse(AUTH_ENDPOINT),
            Uri.parse(TOKEN_ENDPOINT)
        )
        val tokenRequest =
            TokenRequest.Builder(authorizationServiceConfiguration, CLIENT_ID)
                .setRefreshToken(refreshToken).build()

        requestAccessAndRefreshToken(context, tokenRequest)
    }

    private fun authorizeUsingGoogleLogin(context: Context) {
        val serviceConfiguration = getAuthorizationServiceConfiguration()
        val request = getAuthorizationRequest(serviceConfiguration)
        val postAuthorizationIntent = Intent(context, MainActivity::class.java)
        postAuthorizationIntent.putExtra("postAuth", true)

        val pendingIntent = PendingIntent.getActivity(
            context,
            request.hashCode(),
            postAuthorizationIntent,
            0
        )

        authServiceProvider.performAuthorizationRequest(context, request, pendingIntent)
    }

    fun isAuthCallbackIntent(intent: Intent): Boolean = intent.hasExtra("postAuth")

    suspend fun handleAuthCallbackIntent(intent: Intent, context: Context) {
        val response = AuthorizationResponse.fromIntent(intent)
        val error = AuthorizationException.fromIntent(intent)

        if (response == null || error != null) {
            throw Exception("No authorization response available")
        }

        val exchangeTokenRequest = response.createTokenExchangeRequest()
        requestAccessAndRefreshToken(context, exchangeTokenRequest)
    }

    private suspend fun requestAccessAndRefreshToken(
        context: Context,
        tokenRequest: TokenRequest
    ): Nothing? =
        suspendCoroutine { cont ->
            authServiceProvider.performTokenRequest(context, tokenRequest,
                AuthorizationService.TokenResponseCallback { tokenResponse, exception ->
                    authState.update(tokenResponse, exception)

                    if (authState.accessToken == null) {
                        cont.resumeWithException(Exception("Access token is not valid"))
                        return@TokenResponseCallback
                    }

                    authState.refreshToken?.let { authSecretStore.store(context, it) }

                    accessToken = authState.accessToken!!

                    cont.resume(null)
                })
        }

    private fun getAuthorizationServiceConfiguration(): AuthorizationServiceConfiguration {
        return AuthorizationServiceConfiguration(
            Uri.parse(AUTH_ENDPOINT), Uri.parse(TOKEN_ENDPOINT)
        )
    }

    private fun getAuthorizationRequest(serviceConfiguration: AuthorizationServiceConfiguration): AuthorizationRequest {
        val builder = AuthorizationRequest.Builder(
            serviceConfiguration,
            CLIENT_ID,
            ResponseTypeValues.CODE,
            Uri.parse(REDIRECT_URI_STRING)
        )

        builder.setScopes(PROFILE_SCOPE, GCP_SCOPE, DF_SCOPE)

        return builder.build()
    }
}