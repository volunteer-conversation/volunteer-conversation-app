package org.volunteer.conversation.auth

import android.app.PendingIntent
import android.content.Context
import net.openid.appauth.AuthorizationRequest
import net.openid.appauth.AuthorizationService.TokenResponseCallback
import net.openid.appauth.TokenRequest

interface AuthServiceProvider {
    fun performTokenRequest(
        context: Context,
        request: TokenRequest,
        callback: TokenResponseCallback
    )

    fun performAuthorizationRequest(
        context: Context,
        request: AuthorizationRequest,
        completedIntent: PendingIntent
    )
}