package org.volunteer.conversation.auth

import android.content.Context
import android.security.keystore.KeyGenParameterSpec
import android.security.keystore.KeyProperties
import android.util.Base64
import java.security.KeyStore
import javax.crypto.Cipher
import javax.crypto.KeyGenerator
import javax.crypto.SecretKey
import javax.crypto.spec.GCMParameterSpec
import javax.inject.Inject


class AuthSecretStore @Inject constructor() {
    companion object {
        private const val SHARED_PREFERENCES_KEY = "refresh_token"
        private const val ANDROID_KEY_STORE = "AndroidKeyStore"
        private const val KEY_STORE_ALIAS = "refresh_token_key"
        private const val TRANSFORMATION = "AES/GCM/NoPadding"
    }

    private val keyGenerator: KeyGenerator = KeyGenerator
        .getInstance(KeyProperties.KEY_ALGORITHM_AES, ANDROID_KEY_STORE)

    private val keyGenParameterSpec = KeyGenParameterSpec.Builder(
        KEY_STORE_ALIAS,
        KeyProperties.PURPOSE_ENCRYPT or KeyProperties.PURPOSE_DECRYPT
    )
        .setBlockModes(KeyProperties.BLOCK_MODE_GCM)
        .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_NONE)
        .build()

    private val keyStore = KeyStore.getInstance(ANDROID_KEY_STORE).apply {
        load(null)
    }

    init {
        keyGenerator.init(keyGenParameterSpec)
    }

    fun store(context: Context, refreshToken: String) {
        val key = getOrGenerateKey()
        val encryptedRefreshToken = encrypt(refreshToken, key)
        val sharedPreferences = getSharedPreferences(context)
        sharedPreferences.edit().putString(SHARED_PREFERENCES_KEY, encryptedRefreshToken).apply()
    }

    fun retrieveRefreshToken(context: Context): String? {
        return if (doesRefreshTokenKeyExist()) {
            val key = getKey()
            val sharedPreferences = getSharedPreferences(context)
            sharedPreferences.getString(SHARED_PREFERENCES_KEY, null)
                ?.let { encryptedRefreshToken ->
                    decrypt(encryptedRefreshToken, key)
                }
        } else {
            null
        }
    }

    private fun getSharedPreferences(context: Context) =
        context.getSharedPreferences("auth", Context.MODE_PRIVATE)

    private fun getOrGenerateKey(): SecretKey {
        return if (doesRefreshTokenKeyExist()) {
            getKey()
        } else {
            keyGenerator.generateKey()
        }
    }

    private fun getKey(): SecretKey {
        return (keyStore.getEntry(KEY_STORE_ALIAS, null) as KeyStore.SecretKeyEntry).secretKey
    }

    private fun doesRefreshTokenKeyExist() = keyStore.aliases().toList().contains(KEY_STORE_ALIAS)

    private fun encrypt(clearTextSecret: String, key: SecretKey): String {
        val cipher = Cipher.getInstance(TRANSFORMATION)
        cipher.init(Cipher.ENCRYPT_MODE, key)

        val encryptedSecretAsBytes = cipher.doFinal(clearTextSecret.toByteArray())
        val encryptedSecretAsString = Base64.encodeToString(encryptedSecretAsBytes, Base64.DEFAULT)
        val ivAsString = Base64.encodeToString(cipher.iv, Base64.DEFAULT)

        return concat(encryptedSecretAsString, ivAsString)
    }

    private fun decrypt(secretAndIvAsBas64: String, key: SecretKey): String {
        val secretAsBase64 = getSecret(secretAndIvAsBas64)
        val iVAsBase64 = getIv(secretAndIvAsBas64)

        val secretAsBytes = Base64.decode(secretAsBase64, Base64.DEFAULT)
        val ivAsBytes = Base64.decode(iVAsBase64, Base64.DEFAULT)

        val cipher = Cipher.getInstance(TRANSFORMATION)
        val spec = GCMParameterSpec(128, ivAsBytes)
        cipher.init(Cipher.DECRYPT_MODE, key, spec)

        return String(cipher.doFinal(secretAsBytes))
    }

    private fun concat(secret: String, iv: String): String {
        return "$secret,$iv"
    }

    private fun getSecret(secretAndIv: String): String {
        return secretAndIv.split(",")[0]
    }

    private fun getIv(secretAndIv: String): String {
        return secretAndIv.split(",")[1]
    }
}