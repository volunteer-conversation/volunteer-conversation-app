package org.volunteer.conversation.util

import android.util.Log

object Log {
    var shouldMock = false
    fun d(tag: String?, text: String) {
        if (!shouldMock)
            Log.d(tag, text)
        else
            handleLogIfMocked('D', tag, text)
    }

    fun i(tag: String?, text: String) {
        if (!shouldMock)
            Log.i(tag, text)
        else
            handleLogIfMocked('I', tag, text)
    }

    fun w(tag: String?, text: String) {
        if (!shouldMock)
            Log.w(tag, text)
        else
            handleLogIfMocked('W', tag, text)
    }

    private fun handleLogIfMocked(logLevel: Char, tag: String?, text: String) {
        println("$logLevel/${tag ?: ""}: $text")
    }

    fun e(tag: String?, text: String) {
        if (!shouldMock)
            Log.e(tag, text)
        else
            handleLogIfMocked('E', tag, text)
    }
}