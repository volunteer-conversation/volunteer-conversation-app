package org.volunteer.conversation

import android.content.IntentFilter
import android.content.res.Resources
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import org.volunteer.conversation.di.AppComponent
import org.volunteer.conversation.di.DaggerAppComponent
import org.volunteer.conversation.persistence.ConversationRepository
import org.volunteer.conversation.sms.MessageBroadcastReceiver
import org.volunteer.conversation.sms.MessageChatbotConnector
import javax.inject.Inject


class VolunteerConversationApp : DaggerApplication() {

    companion object {
        private const val SMS_RECEIVED = "android.provider.Telephony.SMS_RECEIVED"
        private var instance: VolunteerConversationApp? = null
        fun getResources(): Resources? {
            return instance?.resources
        }
    }

    init {
        instance = this
    }

    lateinit var appComponent: AppComponent

    @Inject
    lateinit var messageReceiver: MessageBroadcastReceiver

    @Inject
    lateinit var messageChatbotConnector: MessageChatbotConnector

    @Inject
    lateinit var conversationRepository: ConversationRepository

    override fun onCreate() {
        super.onCreate()
        registerReceiver(messageReceiver, IntentFilter(SMS_RECEIVED))
        messageChatbotConnector.listenAndRespondToSms()
    }

    override fun applicationInjector(): AndroidInjector<VolunteerConversationApp> {
        val appComponent: AppComponent = DaggerAppComponent.builder()
            .application(this)
            .build()
        appComponent.inject(this)
        this.appComponent = appComponent
        return appComponent
    }
}