package org.volunteer.conversation.ui.volunteer

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fragment_volunteer_list_item.view.*
import org.volunteer.conversation.R
import org.volunteer.conversation.persistence.entities.Volunteer

class VolunteerRecyclerViewAdapter(
    private val volunteersWithConversations: List<Volunteer>,
    private val onVolunteerClickListener: VolunteerFragment.OnListFragmentInteractionListener?
) : RecyclerView.Adapter<VolunteerRecyclerViewAdapter.ViewHolder>() {

    private val onClickListener = View.OnClickListener { v ->
        onVolunteerClickListener?.onListFragmentInteraction(v.tag as Volunteer)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_volunteer_list_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val volunteer = volunteersWithConversations[position]
        with(holder) {
            name.text = volunteer.number
            with(itemView) {
                setOnClickListener(onClickListener)
                tag = volunteersWithConversations[position]
            }
        }
    }

    override fun getItemCount(): Int = volunteersWithConversations.size

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val name: TextView = view.volunteer_name
    }
}
