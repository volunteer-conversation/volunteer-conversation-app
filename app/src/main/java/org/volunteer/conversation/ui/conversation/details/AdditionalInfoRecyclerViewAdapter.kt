package org.volunteer.conversation.ui.conversation.details

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fragment_additional_info_list_item.view.*
import org.volunteer.conversation.R
import java.util.*

class AdditionalInfoRecyclerViewAdapter(
    private val additionalInfos: HashMap<String, String>
) : RecyclerView.Adapter<AdditionalInfoRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_additional_info_list_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val infos = additionalInfos.toList()[position]
        with(holder) {
            firstText.text = infos.first
            secondText.text = infos.second
        }
    }

    override fun getItemCount(): Int = additionalInfos.size

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val firstText: TextView = view.first_text_view
        val secondText: TextView = view.second_text_view
    }
}
