package org.volunteer.conversation.ui.conversation.di

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import org.volunteer.conversation.di.viewmodelfactory.ViewModelKey
import org.volunteer.conversation.ui.conversation.ConversationViewModel
import org.volunteer.conversation.ui.conversation.details.ConversationDetailsViewModel

@Module
abstract class ConversationModule {

    @Binds
    @IntoMap
    @ViewModelKey(ConversationViewModel::class)
    abstract fun bindConversationViewModel(viewModel: ConversationViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ConversationDetailsViewModel::class)
    abstract fun bindConversationDetailsViewModel(viewModel: ConversationDetailsViewModel): ViewModel
}