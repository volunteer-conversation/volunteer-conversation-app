package org.volunteer.conversation.ui.volunteer.di

import dagger.Subcomponent
import org.volunteer.conversation.ui.conversation.di.VolunteerModule
import org.volunteer.conversation.ui.volunteer.VolunteerFragment
import org.volunteer.conversation.ui.volunteer.details.VolunteerDetailsFragment

@Subcomponent(modules = [VolunteerModule::class])
interface VolunteerComponent {

    @Subcomponent.Builder
    interface Builder {
        fun build(): VolunteerComponent
    }

    fun inject(volunteerFragment: VolunteerFragment)
    fun inject(volunteerDetailsFragment: VolunteerDetailsFragment)
}