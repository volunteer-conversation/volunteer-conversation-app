package org.volunteer.conversation.ui.conversation

import android.content.Context
import android.net.Uri
import androidx.core.content.FileProvider
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import org.volunteer.conversation.persistence.ConversationRepository
import org.volunteer.conversation.persistence.entities.Conversation
import org.volunteer.conversation.persistence.entities.ConversationWithVolunteer
import java.io.File
import java.io.OutputStreamWriter
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class ConversationViewModel @Inject constructor(
    private val conversationRepository: ConversationRepository
) : ViewModel() {

    companion object {
        const val COMMA_SEPARATOR = ";"
        const val EXPORT_FILE_EXTENSION = ".csv"
    }

    var conversations: LiveData<List<Conversation>> =
        conversationRepository.getConversationsSortedByCreation().asLiveData()

    fun exportConversations(context: Context, startSharingActivity: (String, String, Uri) -> Unit) {
        viewModelScope.launch {
            val title = getExportFileTitle()
            val fileName = getFileName(title)
            val file = createTempFile(context, fileName)

            val outputStreamWriter = file.writer()
            createCsvHeader(outputStreamWriter)

            conversationRepository.getConversationWithVolunteerSortedByCreation().first()
                .forEach { conversation ->
                    addConversation(conversation, outputStreamWriter)
                }

            outputStreamWriter.close()

            val fileUri: Uri =
                FileProvider.getUriForFile(context, "org.volunteer.conversation.fileprovider", file)

            startSharingActivity(title, fileName + EXPORT_FILE_EXTENSION, fileUri)
        }
    }

    private fun getExportFileTitle(): String {
        val now = Calendar.getInstance().time
        val dateTimeString = formatDateTime(now)

        return "Conversation Export $dateTimeString"
    }

    private fun formatDateTime(date: Date): String {
        val dateTimeFormat =
            SimpleDateFormat("yyyy.MM.dd 'at' HH:mm:ss", Locale.getDefault(Locale.Category.FORMAT))
        return dateTimeFormat.format(date)
    }

    private fun getFileName(title: String): String {
        return title
            .replace(" ", "_")
            .replace(":", "_")
            .replace(".", "_")
            .toLowerCase(Locale.getDefault(Locale.Category.FORMAT))
    }

    private fun createTempFile(context: Context, fileName: String): File {
        val outputDir = context.cacheDir

        return File.createTempFile(
            fileName,
            EXPORT_FILE_EXTENSION, outputDir
        )
    }

    private fun createCsvHeader(outputStreamWriter: OutputStreamWriter) {
        outputStreamWriter.append("Created at")
        outputStreamWriter.append(COMMA_SEPARATOR)
        outputStreamWriter.append("Volunteer name")
        outputStreamWriter.append(COMMA_SEPARATOR)
        outputStreamWriter.append("Volunteer number")
        outputStreamWriter.append(COMMA_SEPARATOR)
        outputStreamWriter.append("Incident")
        outputStreamWriter.append(COMMA_SEPARATOR)
        outputStreamWriter.append("Location")
        outputStreamWriter.append(COMMA_SEPARATOR)
        outputStreamWriter.append("Info")
        outputStreamWriter.append('\n')
    }

    private fun addConversation(
        conversation: ConversationWithVolunteer,
        outputStreamWriter: OutputStreamWriter
    ) {
        outputStreamWriter.append(conversation.conversation.createdAt)
        outputStreamWriter.append(COMMA_SEPARATOR)
        outputStreamWriter.append(conversation.volunteer.name)
        outputStreamWriter.append(COMMA_SEPARATOR)
        outputStreamWriter.append(conversation.volunteer.number)
        outputStreamWriter.append(COMMA_SEPARATOR)
        outputStreamWriter.append(conversation.conversation.incident)
        outputStreamWriter.append(COMMA_SEPARATOR)
        outputStreamWriter.append(conversation.conversation.locationName)
        outputStreamWriter.append(COMMA_SEPARATOR)
        outputStreamWriter.append(conversation.conversation.info)
        outputStreamWriter.append('\n')
    }
}