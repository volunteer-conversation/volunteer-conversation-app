package org.volunteer.conversation.ui.common

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import androidx.core.app.ShareCompat
import javax.inject.Inject

class FileSharer @Inject constructor() {
    fun startSharingActivity(
        activity: Activity,
        chooserTitle: String,
        fileName: String,
        uri: Uri,
        mimeType: String
    ) {
        val sharingIntent = ShareCompat.IntentBuilder.from(activity)
            .setType(mimeType)
            .setSubject(fileName)
            .setChooserTitle(chooserTitle)
            .setStream(uri)
            .createChooserIntent()
            .addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)

        grantPermissions(activity, sharingIntent, uri)
        activity.startActivity(sharingIntent)
    }

    private fun grantPermissions(context: Context, intent: Intent, fileUri: Uri) {
        val resInfoList = context.packageManager
            .queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY)

        for (resolveInfo in resInfoList) {
            val packageName = resolveInfo.activityInfo.packageName
            context.grantUriPermission(
                packageName,
                fileUri,
                Intent.FLAG_GRANT_WRITE_URI_PERMISSION or Intent.FLAG_GRANT_READ_URI_PERMISSION
            )
        }
    }
}