package org.volunteer.conversation.ui.volunteer.details

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fragment_additional_info_list_item.view.*
import org.volunteer.conversation.R
import org.volunteer.conversation.persistence.entities.Conversation

class ConversationRecyclerViewAdapter(
    private val conversations: List<Conversation>,
    private val onConversationClickListener: VolunteerDetailsFragment.OnListFragmentInteractionListener?
) : RecyclerView.Adapter<ConversationRecyclerViewAdapter.ViewHolder>() {

    private val onClickListener = View.OnClickListener { v ->
        onConversationClickListener?.onListFragmentInteraction(v.tag as Conversation)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_volunteer_conversation_list_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val conversation = conversations[position]
        with(holder) {
            with(itemView) {
                setOnClickListener(onClickListener)
                tag = conversation
            }
            firstText.text = conversation.incident
            secondText.text = conversation.createdAt
        }
    }

    override fun getItemCount(): Int = conversations.size

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val firstText: TextView = view.first_text_view
        val secondText: TextView = view.second_text_view
    }
}
