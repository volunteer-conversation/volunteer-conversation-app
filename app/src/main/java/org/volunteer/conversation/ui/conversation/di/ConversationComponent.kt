package org.volunteer.conversation.ui.conversation.di

import dagger.Subcomponent
import org.volunteer.conversation.ui.conversation.ConversationFragment
import org.volunteer.conversation.ui.conversation.details.ConversationDetailsFragment

@Subcomponent(modules = [ConversationModule::class])
interface ConversationComponent {

    @Subcomponent.Builder
    interface Builder {
        fun build(): ConversationComponent
    }

    fun inject(conversationFragment: ConversationFragment)
    fun inject(conversationDetailsFragment: ConversationDetailsFragment)
}