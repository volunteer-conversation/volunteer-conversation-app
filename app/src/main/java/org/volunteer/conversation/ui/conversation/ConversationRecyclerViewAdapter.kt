package org.volunteer.conversation.ui.conversation

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fragment_conversation_list_item.view.*
import org.volunteer.conversation.R
import org.volunteer.conversation.persistence.entities.Conversation

class ConversationRecyclerViewAdapter(
    private val conversationsWithVolunteers: List<Conversation>,
    private val listener: ConversationFragment.OnListFragmentInteractionListener?
) : RecyclerView.Adapter<ConversationRecyclerViewAdapter.ViewHolder>() {

    private val onClickListener = View.OnClickListener { v ->
        listener?.onListFragmentInteraction(v.tag as Conversation)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_conversation_list_item, parent, false)
        return ViewHolder(view)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val conversation = conversationsWithVolunteers[position]
        with(holder) {
            itemIcon.setImageResource(conversation.subject.icon)
            itemText.text = conversation.incident
            itemSecondaryText.text =
                conversation.createdAt + "  \u2014  " + conversation.locationName
            with(itemView) {
                setOnClickListener(onClickListener)
                tag = conversationsWithVolunteers[position]
            }
        }
    }

    override fun getItemCount(): Int = conversationsWithVolunteers.size

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val itemIcon: ImageView = view.image_view
        val itemText: TextView = view.first_text_view
        val itemSecondaryText: TextView = view.second_text_view
    }
}
