package org.volunteer.conversation.ui.volunteer.details

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fragment_additional_info_list_item.view.*
import org.volunteer.conversation.R
import org.volunteer.conversation.persistence.entities.Sms

class SmsRecyclerViewAdapter(
    private val sms: List<Sms>
) : RecyclerView.Adapter<SmsRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_volunteer_sms_list_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val sms = sms[position]
        with(holder) {
            with(itemView) {
                tag = sms
            }
            firstText.text = sms.createdAt
            secondText.text = sms.text
        }
    }

    override fun getItemCount(): Int = sms.size

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val firstText: TextView = view.first_text_view
        val secondText: TextView = view.second_text_view
    }
}
