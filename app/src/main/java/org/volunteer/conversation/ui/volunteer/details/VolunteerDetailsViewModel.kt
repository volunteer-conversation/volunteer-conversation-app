package org.volunteer.conversation.ui.volunteer.details

import android.content.Context
import android.net.Uri
import androidx.core.content.FileProvider
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import kotlinx.coroutines.flow.map
import org.volunteer.conversation.persistence.VolunteerRepository
import org.volunteer.conversation.persistence.entities.Conversation
import org.volunteer.conversation.persistence.entities.Sms
import org.volunteer.conversation.persistence.entities.Volunteer
import java.io.File
import java.io.FileWriter
import javax.inject.Inject

class VolunteerDetailsViewModel @Inject constructor(private val volunteerRepository: VolunteerRepository) :
    ViewModel() {
    companion object {
        const val EXPORT_FILE_EXTENSION = ".vcf"
    }

    fun getConversations(volunteerId: Long): LiveData<List<Conversation>> {
        return volunteerRepository.getVolunteerWithConversations()
            .map { vwcList -> vwcList.first { vwc -> vwc.volunteer.id == volunteerId }.conversations }
            .asLiveData()
    }

    fun getSms(volunteerId: Long): LiveData<List<Sms>> {
        return volunteerRepository.getVolunteerWithSms()
            .map { vwsList -> vwsList.first { vws -> vws.volunteer.id == volunteerId }.sms }
            .asLiveData()
    }

    fun shareVolunteer(
        context: Context,
        volunteer: Volunteer,
        startSharingActivity: (String, String, Uri) -> Unit
    ) {
        val fileName = volunteer.number
        val file = createTempFile(context, fileName)

        val fileWriter = FileWriter(file)

        fileWriter.write("BEGIN:VCARD\r\n")
        fileWriter.write("VERSION:3.0\r\n")
        fileWriter.write("FN:" + volunteer.name + "\r\n")
        fileWriter.write("TEL;TYPE=WORK,VOICE:" + volunteer.number + "\r\n")
        fileWriter.write("END:VCARD\r\n")

        fileWriter.close()

        val fileUri: Uri =
            FileProvider.getUriForFile(context, "org.volunteer.conversation.fileprovider", file)

        startSharingActivity(
            "Share volunteer " + volunteer.number,
            fileName + EXPORT_FILE_EXTENSION,
            fileUri
        )
    }

    private fun createTempFile(context: Context, fileName: String): File {
        val outputDir = context.cacheDir

        return File.createTempFile(
            fileName,
            EXPORT_FILE_EXTENSION, outputDir
        )
    }
}