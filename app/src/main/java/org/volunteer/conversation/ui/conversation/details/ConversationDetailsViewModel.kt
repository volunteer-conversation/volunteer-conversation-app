package org.volunteer.conversation.ui.conversation.details

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import kotlinx.coroutines.flow.map
import org.volunteer.conversation.persistence.ConversationRepository
import org.volunteer.conversation.persistence.entities.Volunteer
import javax.inject.Inject

class ConversationDetailsViewModel @Inject constructor(
    private val conversationRepository: ConversationRepository
) : ViewModel() {
    private val unknownVolunteer = Volunteer(0, "Unknown", "0")

    fun getVolunteerForConversation(conversationId: Long): LiveData<Volunteer> {
        return conversationRepository.getConversationWithVolunteer()
            .map { cwvList ->
                cwvList.find { cwv -> cwv.conversation.id == conversationId }?.volunteer
                    ?: unknownVolunteer
            }.asLiveData()
    }

}