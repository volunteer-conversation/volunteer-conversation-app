package org.volunteer.conversation.ui

import android.Manifest
import android.app.role.RoleManager
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.provider.Telephony
import android.view.MenuItem
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.volunteer.conversation.R
import org.volunteer.conversation.VolunteerConversationApp
import org.volunteer.conversation.auth.AuthService
import org.volunteer.conversation.sms.MessageBroadcastReceiver
import org.volunteer.conversation.sms.MessageChatbotConnector
import org.volunteer.conversation.util.Log
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    companion object {
        private const val VITAL_PERMISSION_REQUEST_CODE = 1234
    }

    @Inject
    lateinit var messageReceiver: MessageBroadcastReceiver

    @Inject
    lateinit var authService: AuthService

    @Inject
    lateinit var messageChatbotConnector: MessageChatbotConnector

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        (applicationContext as VolunteerConversationApp).appComponent.inject(this)
        super.onCreate(savedInstanceState)

        setContentView(R.layout.main_activity)
        //TODO: show loading progress
//        showFragment(loadingFragment)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            requestDefaultSmsAppGraterThanOrEqualQ()
        } else {
            requestDefaultSmsAppSmallerThanQ()
        }

        val context = this
        CoroutineScope(Dispatchers.Main).launch {
            if (authService.isAuthCallbackIntent(intent)) {
                handleAuthCallbackIntent(context)
            }

            if (!authService.isAuthorized()) {
                authService.authorize(context)
            }

            showMainApp()

            if (!isRequiredPermissionsGranted()) {
                requestPermissions()
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.Q)
    private fun requestDefaultSmsAppGraterThanOrEqualQ() {
        val resultLauncher =
            registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
                //TODO: log accept default sms app
            }
        val roleManager = getSystemService(RoleManager::class.java)
        roleManager?.let {
            if (!roleManager.isRoleHeld(RoleManager.ROLE_SMS)) {
                val roleRequestIntent =
                    roleManager.createRequestRoleIntent(RoleManager.ROLE_SMS)
                resultLauncher.launch(roleRequestIntent)
            }
        }
    }

    private fun requestDefaultSmsAppSmallerThanQ() {
        if (isNotDefaultSmsApp()) {
            val intent = Intent(Telephony.Sms.Intents.ACTION_CHANGE_DEFAULT)
            intent.putExtra(Telephony.Sms.Intents.EXTRA_PACKAGE_NAME, packageName)
            startActivity(intent)
        }
    }

    private fun isNotDefaultSmsApp(): Boolean {
        return !this.packageName.equals(Telephony.Sms.getDefaultSmsPackage(this))
    }

    private fun showMainApp() {
        val navView: BottomNavigationView = findViewById(R.id.nav_view)

        val navController = findNavController(R.id.nav_host_fragment)
        val appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.navigation_map, R.id.navigation_conversation, R.id.navigation_volunteers
            )
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)
    }

    private fun showFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
            .replace(R.id.container, fragment)
            .commitNow()
    }

    private suspend fun handleAuthCallbackIntent(
        context: MainActivity
    ) {
        try {
            authService.handleAuthCallbackIntent(intent, context)
        } catch (e: Exception) {
            Log.e(
                this::class.simpleName,
                "Authorization failed, because $e, retrying..."
            )

            if (!authService.isAuthorized()) {
                authService.authorize(context)
            }
        }
    }

    private fun isRequiredPermissionsGranted(): Boolean {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS) ==
                PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.RECEIVE_SMS) ==
                PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.INTERNET) ==
                PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) ==
                PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(
                    this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ) ==
                PackageManager.PERMISSION_GRANTED
    }

    private fun requestPermissions() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(
                Manifest.permission.READ_SMS,
                Manifest.permission.RECEIVE_SMS,
                Manifest.permission.INTERNET,
                Manifest.permission.SEND_SMS,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.ACCESS_FINE_LOCATION
            ),
            VITAL_PERMISSION_REQUEST_CODE
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<String>, grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            VITAL_PERMISSION_REQUEST_CODE -> {
                if (grantResults.isEmpty() || grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    showPermissionExplanation()
                }
            }
        }
    }

    private fun showPermissionExplanation() {
        val builder: AlertDialog.Builder = this.let {
            AlertDialog.Builder(it)
        }

        builder.apply {
            setTitle(R.string.permission_required_diaologe_title)
            setMessage(R.string.permission_required_diaologe_text)
            setPositiveButton(
                R.string.ok
            ) { _, _ ->
                requestPermissions()
            }
            setNegativeButton(
                R.string.cancel
            ) { dialog, _ ->
                dialog.dismiss()
            }
        }

        val dialog: AlertDialog = builder.create()
        dialog.show()
    }
}
