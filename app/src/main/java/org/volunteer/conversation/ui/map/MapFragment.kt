package org.volunteer.conversation.ui.map

import android.content.Context
import android.location.Geocoder
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import org.osmdroid.config.Configuration
import org.osmdroid.tileprovider.tilesource.TileSourceFactory
import org.osmdroid.views.MapView
import org.osmdroid.views.overlay.Marker
import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay
import org.volunteer.conversation.R
import org.volunteer.conversation.VolunteerConversationApp
import org.volunteer.conversation.persistence.entities.LocationWithVolunteers
import javax.inject.Inject


class MapFragment @Inject constructor() : Fragment() {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val viewModel by viewModels<MapViewModel> { viewModelFactory }
    private lateinit var map: MapView

    override fun onAttach(context: Context) {
        super.onAttach(context)
        (requireActivity().application as VolunteerConversationApp).appComponent.mapComponentBuilder.build()
            .inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.fragment_map, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initializeOsm()
        observeLocations()
        viewModel.parseVolunteerLocations(Geocoder(requireContext()))
    }

    private fun initializeOsm() {
        Configuration.getInstance()
            .load(
                requireContext(),
                requireContext().getSharedPreferences(
                    "OSM_SHARED_PREF",
                    Context.MODE_PRIVATE
                )
            )

        map = requireView().findViewById(R.id.map)
        map.setUseDataConnection(true)
        map.setTileSource(TileSourceFactory.MAPNIK)
        map.controller.setZoom(6.0)

        val myLocationOverlay = MyLocationNewOverlay(map)
        myLocationOverlay.enableFollowLocation()
        myLocationOverlay.enableMyLocation()
    }

    private fun observeLocations() {
        viewModel.locationsWithVolunteers.observe(viewLifecycleOwner, Observer {
            if (it.isNotEmpty()) {
                // TODO remove existing markers
                it.filter { it.location != null }.forEach { createMarker(it) }
            }
        })
    }

    private fun createMarker(locationWithVolunteers: LocationWithVolunteers) {
        val marker = Marker(map)
        marker.position = locationWithVolunteers.location
        map.overlays.add(marker)
    }
}
