package org.volunteer.conversation.ui.volunteer

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import org.volunteer.conversation.persistence.VolunteerRepositoryImplementation
import org.volunteer.conversation.persistence.entities.Volunteer
import org.volunteer.conversation.persistence.entities.VolunteerWithConversations
import javax.inject.Inject

class VolunteerViewModel @Inject constructor(
    volunteerRepositoryImplementation: VolunteerRepositoryImplementation
) : ViewModel() {
    var volunteersWithConversations: LiveData<List<VolunteerWithConversations>> =
        volunteerRepositoryImplementation.getVolunteerWithConversations().asLiveData()
    var volunteers: LiveData<List<Volunteer>> =
        volunteerRepositoryImplementation.getVolunteers().asLiveData()
}