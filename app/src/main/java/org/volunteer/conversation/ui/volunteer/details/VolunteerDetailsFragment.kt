package org.volunteer.conversation.ui.volunteer.details

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.provider.ContactsContract
import android.view.*
import androidx.core.app.ShareCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_conversation_details.volunteer_name
import kotlinx.android.synthetic.main.fragment_conversation_details.volunteer_number
import kotlinx.android.synthetic.main.fragment_volunteer_details.*
import org.volunteer.conversation.R
import org.volunteer.conversation.VolunteerConversationApp
import org.volunteer.conversation.persistence.entities.Conversation
import org.volunteer.conversation.persistence.entities.Volunteer
import org.volunteer.conversation.ui.common.FileSharer
import javax.inject.Inject


class VolunteerDetailsFragment @Inject constructor() : Fragment() {
    @Inject
    lateinit var fileSharer: FileSharer

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val viewModel by viewModels<VolunteerDetailsViewModel> { viewModelFactory }
    private val args: VolunteerDetailsFragmentArgs by navArgs()

    private val onConversationClickListener = object :
        OnListFragmentInteractionListener {
        override fun onListFragmentInteraction(item: Conversation) {
            val action =
                VolunteerDetailsFragmentDirections.actionNavigationVolunteerDetailsToNavigationConversationDetails(
                    item
                )
            findNavController().navigate(action)
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        (requireActivity().application as VolunteerConversationApp).appComponent.volunteerComponent.build()
            .inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        setHasOptionsMenu(true)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_volunteer_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(args.volunteer) {
            volunteer_name.text = name
            volunteer_number.text = number

            conversation_list.layoutManager = LinearLayoutManager(context)
            sms_list.layoutManager = LinearLayoutManager(context)

            viewModel.getConversations(id!!).observe(viewLifecycleOwner, Observer {
                conversation_list.adapter =
                    ConversationRecyclerViewAdapter(it, onConversationClickListener)
            })

            viewModel.getSms(id!!).observe(viewLifecycleOwner, Observer {
                sms_list.adapter =
                    SmsRecyclerViewAdapter(it)
            })

        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.volunteer_details_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.share_volunteer -> {
                shareVolunteer()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun shareVolunteer() {
        with(args.volunteer) {
            viewModel.shareVolunteer(
                requireContext(),
                this
            ) { chooserTitle: String, fileName: String, uri: Uri ->
                fileSharer.startSharingActivity(
                    requireActivity(),
                    chooserTitle,
                    fileName,
                    uri,
                    ContactsContract.Contacts.CONTENT_VCARD_TYPE
                )
            }
        }
    }

    private fun startSharingActivity(
        volunteer: Volunteer,
        fileName: String,
        uri: Uri
    ) {
        val sharingIntent = ShareCompat.IntentBuilder.from(requireActivity())
            .setType(ContactsContract.Contacts.CONTENT_VCARD_TYPE)
            .setSubject(fileName)
            .setChooserTitle("Share volunteer " + volunteer.number)
            .setStream(uri)
            .createChooserIntent()
            .addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)

        grantPermissions(sharingIntent, uri)
        requireContext().startActivity(sharingIntent)
    }

    private fun grantPermissions(intent: Intent, fileUri: Uri) {
        val resInfoList = requireContext().packageManager
            .queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY)

        for (resolveInfo in resInfoList) {
            val packageName = resolveInfo.activityInfo.packageName
            requireContext().grantUriPermission(
                packageName,
                fileUri,
                Intent.FLAG_GRANT_WRITE_URI_PERMISSION or Intent.FLAG_GRANT_READ_URI_PERMISSION
            )
        }
    }

    interface OnListFragmentInteractionListener {
        fun onListFragmentInteraction(item: Conversation)
    }
}
