package org.volunteer.conversation.ui.map.di

import dagger.Subcomponent
import org.volunteer.conversation.ui.map.MapFragment

@Subcomponent(modules = [MapModule::class])
interface MapComponent {

    @Subcomponent.Builder
    interface Builder {
        fun build(): MapComponent
    }

    fun inject(mapFragment: MapFragment)

}