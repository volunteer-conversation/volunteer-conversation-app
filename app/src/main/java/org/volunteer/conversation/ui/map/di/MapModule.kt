package org.volunteer.conversation.ui.map.di

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import org.volunteer.conversation.di.viewmodelfactory.ViewModelKey
import org.volunteer.conversation.ui.map.MapViewModel

@Module
abstract class MapModule {
    @Binds
    @IntoMap
    @ViewModelKey(MapViewModel::class)
    abstract fun bindViewModel(viewmodel: MapViewModel): ViewModel
}