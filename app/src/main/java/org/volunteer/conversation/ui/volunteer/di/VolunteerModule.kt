package org.volunteer.conversation.ui.conversation.di

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import org.volunteer.conversation.di.viewmodelfactory.ViewModelKey
import org.volunteer.conversation.ui.volunteer.VolunteerViewModel
import org.volunteer.conversation.ui.volunteer.details.VolunteerDetailsViewModel

@Module
abstract class VolunteerModule {

    @Binds
    @IntoMap
    @ViewModelKey(VolunteerViewModel::class)
    abstract fun bindVolunteerViewModel(viewmodel: VolunteerViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(VolunteerDetailsViewModel::class)
    abstract fun bindVolunteerDetailsViewModel(viewmodel: VolunteerDetailsViewModel): ViewModel
}