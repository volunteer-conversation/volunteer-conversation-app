package org.volunteer.conversation.ui.conversation

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_conversation_list.*
import org.volunteer.conversation.R
import org.volunteer.conversation.VolunteerConversationApp
import org.volunteer.conversation.persistence.entities.Conversation
import org.volunteer.conversation.ui.common.FileSharer
import javax.inject.Inject


class ConversationFragment @Inject constructor() : Fragment() {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val viewModel by viewModels<ConversationViewModel> { viewModelFactory }

    @Inject
    lateinit var fileSharer: FileSharer

    private val listener = object : OnListFragmentInteractionListener {
        override fun onListFragmentInteraction(item: Conversation) {
            val action =
                ConversationFragmentDirections.actionNavigationConversationToNavigationConversationDetails(
                    item
                )
            findNavController().navigate(action)
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        (requireActivity().application as VolunteerConversationApp).appComponent.conversationComponent.build()
            .inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_conversation, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        setHasOptionsMenu(true)
        super.onCreate(savedInstanceState)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.export_conversation -> {
                exportConversations()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        conversation_list.layoutManager = LinearLayoutManager(context)

        viewModel.conversations.observe(viewLifecycleOwner, Observer { c ->
            conversation_list.adapter =
                ConversationRecyclerViewAdapter(c, listener)
        })
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.conversation_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    private fun exportConversations() {
        viewModel.exportConversations(requireContext()) { title: String, fileName: String, uri: Uri ->
            fileSharer.startSharingActivity(requireActivity(), title, fileName, uri, "*/*")
        }
    }

    interface OnListFragmentInteractionListener {
        fun onListFragmentInteraction(item: Conversation)
    }
}
