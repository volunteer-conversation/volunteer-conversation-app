package org.volunteer.conversation.ui.conversation.details

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.fragment_conversation_details.*
import org.volunteer.conversation.R
import org.volunteer.conversation.VolunteerConversationApp
import java.util.*
import javax.inject.Inject


class ConversationDetailsFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val viewModel by viewModels<ConversationDetailsViewModel> { viewModelFactory }

    private val args: ConversationDetailsFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_conversation_details, container, false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        (requireActivity().application as VolunteerConversationApp).appComponent.conversationComponent.build()
            .inject(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(args.conversation) {
            subject_icon.setImageResource(subject.icon)
            incident_title.text = incident
            subject_title.text = subject.name
            location_title.text = locationName
            date_title.text = createdAt
        }

        viewModel.getVolunteerForConversation(args.conversation.id!!)
            .observe(viewLifecycleOwner, Observer {
                volunteer_name.text = it.name
                volunteer_number.text = it.number
            })

        val additionalInfo = Gson().fromJson<HashMap<String, String>>(
            args.conversation.info,
            object : TypeToken<HashMap<String, String>>() {}.type
        )
        with(additional_info_list) {
            layoutManager = LinearLayoutManager(context)
            adapter = AdditionalInfoRecyclerViewAdapter(additionalInfo)
        }
    }
}
