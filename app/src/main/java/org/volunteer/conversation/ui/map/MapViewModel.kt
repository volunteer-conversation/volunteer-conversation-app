package org.volunteer.conversation.ui.map

import android.location.Geocoder
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.osmdroid.util.GeoPoint
import org.volunteer.conversation.persistence.VolunteerRepository
import org.volunteer.conversation.persistence.entities.LocationWithVolunteers
import javax.inject.Inject

class MapViewModel @Inject constructor(private val volunteerRepository: VolunteerRepository) :
    ViewModel() {
    private val mutableLocationsWithVolunteers =
        MutableLiveData<List<LocationWithVolunteers>>()

    val locationsWithVolunteers: LiveData<List<LocationWithVolunteers>> =
        mutableLocationsWithVolunteers

    fun parseVolunteerLocations(geocoder: Geocoder) {
        viewModelScope.launch {
            volunteerRepository.getVolunteerLocations().collect {
                val newUnparsedLocationsWithVolunteers = filterEmpty(filterExisting(it))
                newUnparsedLocationsWithVolunteers.forEach {
                    val newLocationWithVolunteers = queryNewLocation(geocoder, it)
                    updateLocationsWithVolunteers(newLocationWithVolunteers)
                }
            }
        }
    }

    private fun filterExisting(unparsedLocationsWithVolunteers: List<LocationWithVolunteers>) =
        unparsedLocationsWithVolunteers.filter { !doesUnparsedLocationExist(it.locationName) }

    private fun doesUnparsedLocationExist(unparsedLocation: String): Boolean {
        return if (locationsWithVolunteers.value == null) {
            false
        } else {
            locationsWithVolunteers.value!!.find { it.locationName == unparsedLocation } != null
        }
    }

    private fun filterEmpty(unparsedLocationsWithVolunteers: List<LocationWithVolunteers>) =
        unparsedLocationsWithVolunteers.filter { it.locationName.isNotEmpty() }

    private suspend fun queryNewLocation(
        geocoder: Geocoder,
        newUnparsedLocationWithVolunteers: LocationWithVolunteers
    ) = withContext(Dispatchers.IO) {
        val locations =
            geocoder.getFromLocationName(newUnparsedLocationWithVolunteers.locationName, 1)

        if (locations.isNotEmpty()) {
            val location = locations.first()
            return@withContext LocationWithVolunteers(
                locationName = newUnparsedLocationWithVolunteers.locationName,
                location = GeoPoint(location.latitude, location.longitude),
                volunteers = newUnparsedLocationWithVolunteers.volunteers
            )
        }

        return@withContext null
    }

    private fun updateLocationsWithVolunteers(newLocationWithVolunteers: LocationWithVolunteers?) {
        if (newLocationWithVolunteers != null) {
            mutableLocationsWithVolunteers.value =
                locationsWithVolunteers.value?.plus(newLocationWithVolunteers)
                    ?: listOf(newLocationWithVolunteers)
        }
    }
}
