package org.volunteer.conversation.ui.volunteer

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_volunteer_list.*
import org.volunteer.conversation.R
import org.volunteer.conversation.VolunteerConversationApp
import org.volunteer.conversation.persistence.entities.Volunteer
import javax.inject.Inject

class VolunteerFragment @Inject constructor() : Fragment() {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val viewModel by viewModels<VolunteerViewModel> { viewModelFactory }

    private val onVolunteerClickListener = object : OnListFragmentInteractionListener {
        override fun onListFragmentInteraction(item: Volunteer) {
            val action =
                VolunteerFragmentDirections.actionNavigationVolunteersToNavigationVolunteerDetails(
                    item
                )
            findNavController().navigate(action)
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        (requireActivity().application as VolunteerConversationApp).appComponent.volunteerComponent.build()
            .inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_volunteers, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        volunteer_list.layoutManager = LinearLayoutManager(context)

        viewModel.volunteers.observe(viewLifecycleOwner, Observer {
            volunteer_list.adapter =
                VolunteerRecyclerViewAdapter(it, onVolunteerClickListener)
        })
    }

    interface OnListFragmentInteractionListener {
        fun onListFragmentInteraction(item: Volunteer)
    }
}
