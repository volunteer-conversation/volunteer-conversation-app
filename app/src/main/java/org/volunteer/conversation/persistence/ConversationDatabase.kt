package org.volunteer.conversation.persistence

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import org.volunteer.conversation.persistence.daos.ConversationDao
import org.volunteer.conversation.persistence.daos.SmsDao
import org.volunteer.conversation.persistence.daos.VolunteerDao
import org.volunteer.conversation.persistence.entities.Conversation
import org.volunteer.conversation.persistence.entities.Sms
import org.volunteer.conversation.persistence.entities.Volunteer

@Database(
    entities = [Conversation::class, Volunteer::class, Sms::class],
    version = 1,
    exportSchema = false
)
@TypeConverters(Converters::class)
abstract class ConversationDatabase : RoomDatabase() {
    abstract fun conversationDao(): ConversationDao
    abstract fun volunteerDao(): VolunteerDao
    abstract fun smsDao(): SmsDao
}