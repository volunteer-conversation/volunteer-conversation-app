package org.volunteer.conversation.persistence

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import org.volunteer.conversation.chatbot.dtos.QueryResultLocation
import org.volunteer.conversation.persistence.entities.Conversation
import org.volunteer.conversation.persistence.entities.ConversationWithVolunteer
import org.volunteer.conversation.persistence.entities.Volunteer
import org.volunteer.conversation.util.Log
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ConversationRepository @Inject constructor(private val conversationDatabase: ConversationDatabase) {
    fun getConversationWithVolunteer(): Flow<List<ConversationWithVolunteer>> =
        conversationDatabase.conversationDao().getConversationWithVolunteers()

    fun getConversationWithVolunteerSortedByCreation(): Flow<List<ConversationWithVolunteer>> =
        conversationDatabase.conversationDao().getConversationWithVolunteers()
            .map { cwv -> cwv.sortedByDescending { c -> c.conversation.createdAtTimestamp } }

    fun getConversationsSortedByCreation(): Flow<List<Conversation>> =
        conversationDatabase.conversationDao().getConversation()
            .map { conversation -> conversation.sortedByDescending { c -> c.createdAtTimestamp } }

    suspend fun insertConversation(volunteer: Volunteer, conversation: Conversation) {
        var vId = conversationDatabase.volunteerDao().insertVolunteer(volunteer)
        if (!entityInserted(vId)) {
            val persistedVolunteer =
                conversationDatabase.volunteerDao().getVolunteerByNumber(volunteer.number).get()
            vId = persistedVolunteer.id!!
            checkIfVolunteerHasNameAndUpdateIfNot(persistedVolunteer, volunteer)
        }

        conversation.volunteerId = vId
        conversationDatabase.conversationDao().insertConversation(conversation)
    }

    private suspend fun checkIfVolunteerHasNameAndUpdateIfNot(
        persistedVolunteer: Volunteer,
        newVolunteer: Volunteer
    ) {
        if (persistedVolunteer.name.isNullOrEmpty()) {
            persistedVolunteer.name = newVolunteer.name
            conversationDatabase.volunteerDao().updateVolunteer(persistedVolunteer)
        }
    }

    //TODO: this is a little bit dangerous since it can potentially overwrite the location of an
    // conversation which has already been persisted if the insertConversation() method fails
    suspend fun insertLocationForLastConversationIfVolunteerExists(
        volunteer: Volunteer,
        location: QueryResultLocation
    ) {
        val optVolunteer =
            conversationDatabase.volunteerDao().getVolunteerByNumber(volunteer.number)
        if (optVolunteer.isPresent) {
            val vId = optVolunteer.get().id!!
            insertLocationForLastConversationIfConversationExists(vId, location.city)
        } else {
            Log.e(
                this::class.simpleName,
                "Could not find volunteer with number ${volunteer.number}."
            )
        }
    }

    private suspend fun insertLocationForLastConversationIfConversationExists(
        vId: Long,
        city: String
    ) {
        if (entityExists(vId)) {
            val optConversation = conversationDatabase.conversationDao().getLatestConversation(vId)
            if (optConversation.isPresent) {
                val c = optConversation.get()
                c.locationName = city
                conversationDatabase.conversationDao().updateConversation(c)
            } else {
                Log.e(
                    this::class.simpleName,
                    "Could not find conversation for volunteer with number ${vId}."
                )
            }
        }
    }

    private fun entityExists(id: Long?) = id != null && id != -1L
    private fun entityInserted(id: Long?) = entityExists(id)
}