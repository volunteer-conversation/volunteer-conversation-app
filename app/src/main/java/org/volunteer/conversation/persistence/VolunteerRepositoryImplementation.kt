package org.volunteer.conversation.persistence

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import org.volunteer.conversation.persistence.entities.LocationWithVolunteers
import org.volunteer.conversation.persistence.entities.Volunteer
import org.volunteer.conversation.persistence.entities.VolunteerWithConversations
import org.volunteer.conversation.persistence.entities.VolunteerWithSms
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class VolunteerRepositoryImplementation @Inject constructor(private val conversationDatabase: ConversationDatabase) :
    VolunteerRepository {
    override fun getVolunteers(): Flow<List<Volunteer>> =
        conversationDatabase.volunteerDao().getVolunteers()

    override fun getVolunteerWithSms(): Flow<List<VolunteerWithSms>> =
        conversationDatabase.volunteerDao().getVolunteerWithSms()

    override fun getVolunteerWithConversations(): Flow<List<VolunteerWithConversations>> =
        conversationDatabase.volunteerDao().getVolunteerWithConversation()

    override fun getVolunteerLocations(): Flow<List<LocationWithVolunteers>> {
        val conversationsWithVolunteerFlow =
            conversationDatabase.conversationDao().getConversationWithVolunteers()

        val conversationsWithVolunteerPerLocation =
            conversationsWithVolunteerFlow.map { it.groupBy { it.conversation.locationName } }

        return conversationsWithVolunteerPerLocation.map {
            it.map {
                LocationWithVolunteers(
                    it.key,
                    volunteers = it.value.map { it.volunteer })
            }
        }
    }
}