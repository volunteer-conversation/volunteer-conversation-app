package org.volunteer.conversation.persistence.di

import android.app.Application
import androidx.room.Room
import dagger.Module
import dagger.Provides
import org.volunteer.conversation.persistence.ConversationDatabase
import org.volunteer.conversation.persistence.VolunteerRepository
import org.volunteer.conversation.persistence.VolunteerRepositoryImplementation
import javax.inject.Singleton

@Module
object PersistenceModule {

    @JvmStatic
    @Singleton
    @Provides
    fun provideDataBase(application: Application): ConversationDatabase {
        return Room.databaseBuilder(
            application.applicationContext,
            ConversationDatabase::class.java,
            "Conversation.db"
        ).build()
    }

    @Provides
    fun inject(volunteerRepositoryImplementation: VolunteerRepositoryImplementation): VolunteerRepository =
        volunteerRepositoryImplementation
}