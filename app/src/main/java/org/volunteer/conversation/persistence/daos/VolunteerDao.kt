package org.volunteer.conversation.persistence.daos

import androidx.room.*
import kotlinx.coroutines.flow.Flow
import org.volunteer.conversation.persistence.entities.Volunteer
import org.volunteer.conversation.persistence.entities.VolunteerWithConversations
import org.volunteer.conversation.persistence.entities.VolunteerWithSms
import java.util.*

@Dao
abstract class VolunteerDao {
    @Query("SELECT * FROM volunteer")
    abstract fun getVolunteers(): Flow<List<Volunteer>>

    @Query("SELECT * FROM volunteer WHERE number=:number")
    abstract suspend fun getVolunteerByNumber(number: String): Optional<Volunteer>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    abstract suspend fun insertVolunteer(volunteer: Volunteer): Long

    @Update
    abstract suspend fun updateVolunteer(volunteer: Volunteer)

    @Query("SELECT * FROM volunteer")
    abstract fun getVolunteerWithConversation(): Flow<List<VolunteerWithConversations>> //TODO: dao unit test

    @Query("SELECT * FROM volunteer")
    abstract fun getVolunteerWithSms(): Flow<List<VolunteerWithSms>>  //TODO: dao unit test
}