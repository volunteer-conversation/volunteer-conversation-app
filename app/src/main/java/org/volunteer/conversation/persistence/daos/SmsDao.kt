package org.volunteer.conversation.persistence.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import kotlinx.coroutines.flow.Flow
import org.volunteer.conversation.persistence.entities.Sms

@Dao
abstract class SmsDao {
    @Query("SELECT * FROM sms")
    abstract fun getSms(): Flow<List<Sms>>

    suspend fun insertSms(sms: Sms): Long {
        return insert(sms.apply { createdAtTimestamp = System.currentTimeMillis() })
    }

    @Insert
    protected abstract suspend fun insert(conversation: Sms): Long
}