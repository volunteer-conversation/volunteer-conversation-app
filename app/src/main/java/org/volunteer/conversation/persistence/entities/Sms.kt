package org.volunteer.conversation.persistence.entities

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize
import java.text.SimpleDateFormat
import java.util.*

@Parcelize
@Entity(
    foreignKeys = [
        ForeignKey(
            entity = Volunteer::class,
            parentColumns = ["id"],
            childColumns = ["volunteerId"],
            onUpdate = ForeignKey.CASCADE
        ),
        ForeignKey(
            entity = Conversation::class,
            parentColumns = ["id"],
            childColumns = ["conversationId"],
            onUpdate = ForeignKey.CASCADE
        )]
)
data class Sms(
    @PrimaryKey(autoGenerate = true) var id: Long? = null,
    @ColumnInfo(name = "text") var text: String,
    @ColumnInfo(name = "volunteerId") var volunteerId: Long? = null, //TODO: naming
    @ColumnInfo(name = "conversationId") var conversationId: Long? = null, //TODO: naming
    @ColumnInfo(name = "created_at") var createdAtTimestamp: Long? = null
) : Parcelable {
    //TODO: use Room Converters
    val createdAt: String
        get() {
            val sdf = SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.US)
            val date = Date(createdAtTimestamp ?: 0)
            return sdf.format(date)
        }
}