package org.volunteer.conversation.persistence

import androidx.room.TypeConverter
import org.volunteer.conversation.persistence.entities.Subject

object Converters {
    @TypeConverter
    @JvmStatic
    fun subjectToString(subject: Subject) = subject.name

    @TypeConverter
    @JvmStatic
    fun stringToSubject(s: String) = Subject.valueOf(s)

}