package org.volunteer.conversation.persistence.entities

import org.volunteer.conversation.R

enum class Subject {
    ALERT {
        override val icon = R.drawable.ic_alert_black_24dp
    },
    REPORT {
        override val icon = R.drawable.ic_report_black_24dp
    },
    ZERO_ALERT {
        override val icon = R.drawable.ic_zero_black_24dp
    };

    abstract val icon: Int
}
