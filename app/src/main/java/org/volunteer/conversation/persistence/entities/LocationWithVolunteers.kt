package org.volunteer.conversation.persistence.entities

import org.osmdroid.util.GeoPoint

data class LocationWithVolunteers(
    val locationName: String = "",
    val location: GeoPoint? = null,
    val volunteers: List<Volunteer>
)