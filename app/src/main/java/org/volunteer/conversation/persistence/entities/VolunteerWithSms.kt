package org.volunteer.conversation.persistence.entities

import android.os.Parcelable
import androidx.room.Embedded
import androidx.room.Relation
import kotlinx.android.parcel.Parcelize

@Parcelize
data class VolunteerWithSms(
    @Embedded val volunteer: Volunteer,
    @Relation(parentColumn = "id", entityColumn = "volunteerId")
    val sms: List<Sms>
) : Parcelable