package org.volunteer.conversation.persistence.daos

import androidx.room.*
import kotlinx.coroutines.flow.Flow
import org.volunteer.conversation.persistence.entities.Conversation
import org.volunteer.conversation.persistence.entities.ConversationWithVolunteer
import java.util.*

@Dao
abstract class ConversationDao {
    @Query("SELECT * FROM conversation")
    abstract fun getConversation(): Flow<List<Conversation>>

    suspend fun insertConversation(conversation: Conversation): Long {
        return insert(conversation.apply { createdAtTimestamp = System.currentTimeMillis() })
    }

    @Update
    abstract suspend fun updateConversation(conversation: Conversation)

    @Insert
    protected abstract suspend fun insert(conversation: Conversation): Long

    @Transaction
    @Query("SELECT * FROM conversation")
    abstract fun getConversationWithVolunteers(): Flow<List<ConversationWithVolunteer>>

    @Query("SELECT * FROM conversation WHERE volunteerId == :volunteerId ORDER BY id DESC LIMIT 1")
    abstract suspend fun getLatestConversation(volunteerId: Long): Optional<Conversation> //TODO: Unit dao test is missing

}