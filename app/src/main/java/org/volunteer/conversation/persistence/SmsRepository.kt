package org.volunteer.conversation.persistence

import org.volunteer.conversation.persistence.entities.Sms
import org.volunteer.conversation.persistence.entities.Volunteer
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SmsRepository @Inject constructor(private val conversationDatabase: ConversationDatabase) {

    suspend fun insertSms(volunteer: Volunteer, sms: Sms) {
        var vId = conversationDatabase.volunteerDao().insertVolunteer(volunteer)
        if (!entityInserted(vId)) {
            vId = conversationDatabase.volunteerDao().getVolunteerByNumber(volunteer.number)
                .get().id!!
        }

        sms.volunteerId = vId
        conversationDatabase.smsDao().insertSms(sms)
    }

    private fun entityInserted(id: Long?) = entityExists(id)
    private fun entityExists(id: Long?) = id != null && id != -1L
}