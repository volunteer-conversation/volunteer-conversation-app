package org.volunteer.conversation.persistence.entities

import android.os.Parcelable
import androidx.room.Embedded
import androidx.room.Relation
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ConversationWithVolunteer(
    @Embedded val conversation: Conversation,
    @Relation(parentColumn = "volunteerId", entityColumn = "id")
    val volunteer: Volunteer
) : Parcelable