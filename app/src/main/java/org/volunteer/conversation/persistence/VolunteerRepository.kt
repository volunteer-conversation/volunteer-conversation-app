package org.volunteer.conversation.persistence

import kotlinx.coroutines.flow.Flow
import org.volunteer.conversation.persistence.entities.LocationWithVolunteers
import org.volunteer.conversation.persistence.entities.Volunteer
import org.volunteer.conversation.persistence.entities.VolunteerWithConversations
import org.volunteer.conversation.persistence.entities.VolunteerWithSms

interface VolunteerRepository {
    fun getVolunteers(): Flow<List<Volunteer>>
    fun getVolunteerWithSms(): Flow<List<VolunteerWithSms>>
    fun getVolunteerWithConversations(): Flow<List<VolunteerWithConversations>>
    fun getVolunteerLocations(): Flow<List<LocationWithVolunteers>>
}