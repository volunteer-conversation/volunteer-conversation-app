package org.volunteer.conversation.systemapp

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

/** Do not delete this class since it is necessary to let the app be a SMS default system app.
 * However, feel free to implement this class if you want to provide such functionality.
 */
class MmsReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        println(intent.toString())
    }
}