package org.volunteer.conversation.systemapp

import android.app.Service
import android.content.Intent
import android.os.IBinder

/** Do not delete this class since it is necessary to let the app be a SMS default system app.
 * However, feel free to implement this class if you want to provide such functionality.
 */
class HeadlessSmsSendService : Service() {
    override fun onBind(intent: Intent): IBinder? {
        return null
    }
}
