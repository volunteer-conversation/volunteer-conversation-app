package org.volunteer.conversation.systemapp

import android.app.Activity

/** Do not delete this class since it is necessary to let the app be a SMS default system app.
 * However, feel free to implement this class if you want to provide such functionality.
 */
class ComposeSmsActivity : Activity()