# Volunteer Conversation App
                                                                                                         
This Android app acts as a SMS chatbot. It reads SMS, process them, and a Google Dialogflow Chatbot generates appropriate answers. The Volunteer Conversation App can be used to simplify the communication between association and their volunteers.

## Contents

- [Our use case](#our-use-case)
- [Adapt use case](#adapt-use-case)
- [Create your own conversation app](#create-your-own-conversation-app)
- [Contact](#contact)
- [License](#license)

## Our use case

In our case the customer has over 5000 volunteers. The volunteers are part of communities spread over the country, especially in rural and remote areas. The association`s head office asks them to report floods, landslides, accidents and certain kind of diseases. Internet access can be challenging, due to bad network coverage and/or missing hardware. Luckily, this application is a perfect fit for the association's needs. Their volunteers can text ordinary SMS to the phone running this application and the phone will act as a chatbot and collect all necessary information from the volunteer.

| Server App | Volunteer Client Phone |
| ------ | ------ |
| ![](docs/server-app.png) | ![](docs/client-app.png) |

Finally, all conversations can be exported and analysed on any computer.

![](docs/csv-export.png)

## Adapt use case

The Google Dialogflow chatbot [deployment located in this repository](dialogflow-agent) handles only a limited number of possible intents.

![](docs/intents.png)

However, this intents can easily be changed or deleted and new intents can be added. Go to [Dialogflow](https://dialogflow.cloud.google.com/) change, delete or add intents, export the chatbot and add it to your fork of this repository, by replacing [the dialogflow-agent directory](dialogflow-agent). You could add intents like "water level", "child born" or "rare animal spotted". No need to change anything at your instance of the Android application.

![](docs/export.png)

## Create your own conversation app

This Android app does not have a traditional backend. It uses Google Dialogflow as a chatbot engine, though. You can create your own, free Google Dialogflow chatbot easily:

1. Fork this Gitlab project
1. Create a new chatbot at [Dialogflow](https://dialogflow.cloud.google.com/)
1. [Set up authentication with a service account](https://cloud.google.com/docs/authentication/getting-started) and download the JSON service key; Assign the role `Owner` to the service account
1. Create a Gitlab CI variable `GOOGLE_CREDENTIALS` and add the JSON service key file content
1. Find your chatbot ID at [Dialogflow](https://dialogflow.cloud.google.com/) and add it to [dialogflow_chatbot.xml](app/src/main/res/values/dialogflow_chatbot.xml) as well as to `googleAssistant.project` in [agent.json](dialogflow-agent/agent.json)
1. Run the Gitlab CI of your forked project
1. If you want to allow somebody else than yourself to use your instance of the Volunteer Conversation app, add her/him to the Google Cloud Project in which your Google Dialogflow chatbot is located in via the [GCP Console](https://console.cloud.google.com/iam-admin/iam); Assign the third party user at least the role `Dialogflow API Client`

## Contact

If you have any feedback, troubles, ideas or the need to talk, don't hesitate to contact one of the two or both authors of this application:
- Philipp Moser, moserp1991@gmail.com
- Marlon Alagoda, marlon@alagoda.at

## License

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
